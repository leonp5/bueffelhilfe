const mix = require("laravel-mix");
// const browserSync = require("browser-sync");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/assets/js/app.js", "public/js").sass(
    "resources/assets/sass/app.scss",
    "public/css"
);

mix.copyDirectory("resources/assets/fonts", "public/fonts").copyDirectory(
    "resources/assets/js/components/contactMap",
    "public/js/components/contactMap"
);

mix.browserSync({
    proxy: "http://127.0.0.1:8000",

    files: [
        "resources/**/*.scss",
        "resources/**/*.js",
        "resources/**/*.php",
        "routes/*.php",
        "app/**/*.php"
    ],
    // => true = the browse opens a new browser window with every npm run watch startup/reload

    open: false,
    notify: false,
    ui: false
});
