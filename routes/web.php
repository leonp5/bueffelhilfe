<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PagesController@main")->name("main");
Route::get("/reports", "ReportsController@index")->name("reports");
Route::get("/privacy", "PagesController@privacy")->name("privacy");
Route::get("/disclaimer", "PagesController@disclaimer")->name("disclaimer");

// Disabling Auth::routes() and only make login and logout accessible

Route::get('login', 'Auth\LoginController@login')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Protected routes

// Show administration

// Route::middleware("can:administration")->name("admin")->group(
//   function () {
//     Route::get("/administration", "PagesController@index");
//   }
// );

// namespace directs to the path/folder
// prefix = adds the word to the URL
// name = adds the word to the routes (helpful for app structuring in bigger apps)
// middleware takes the gate "administration" from AuthServiceProvider

// Show, edit & delete users

Route::namespace("Admin")->prefix("admin")->name("admin.")->middleware("can:administration")->group(function () {
  Route::resource("/users", "UsersController", ["except" => ["create", "show", "store"]]);
});

// Add users

Route::namespace("Auth")->prefix("admin/user")->middleware("can:administration")->group(function () {

  Route::get("/add", "RegisterController@showRegistrationForm")->name("admin.user.create");
  Route::post("/add", "RegisterController@register");
});

// Add, edit & delete reports, edit content from main page 

Route::prefix("admin")->middleware("can:administration")->name("admin.")->group(function () {
  Route::match(['put', 'patch'], "reports/multiupdate", "ReportsController@MultipleUpate")->name("reports.multiupdate");
  Route::match(['put', 'patch'], "reports/checkboxupdate", "ReportsController@CheckBoxUpdate")->name("reports.checkboxupdate");
  Route::resource("/reports", "ReportsController", ["except" => ["create", "show"]]);
  Route::resource("/content", "ContentController", ["except" => ["index", "show", "create", "destroy", "store"]]);
});

// Show & download files

Route::middleware("auth")->group(function () {

  Route::get("/folders", "FileController@index")->name("files.index");
  Route::get("files/{file}", "FileController@showCommonFiles")->name("files.show");
  Route::get("/document/{id}", "FileController@downloadSingleFile")->name("file.get");
  Route::get("/personalfiles", "FileController@showPersonalFiles")->name("personalFiles.show");
});

// Add, edit, move & delete files

Route::middleware("can:administration")->name("admin.")->group(function () {
  Route::resource("/admin/file", "FileController", ["except" => ["index", "show", "destroy"]]);
  Route::get("/userfiles/{id}", "FileController@showUserFiles")->name("userFiles.show");
  Route::get("/admin/folder/file/{id}", "FileController@addFileToFolder")->name("addFileToFolder");
  Route::match(["put", "delete"], "filesDeleteOrMove", "FileController@filesDeleteOrMove")->name("filesDeleteOrMove");
});

// Add, edit & delete folder

Route::middleware("can:administration")->name("admin.")->group(function () {
  Route::resource("/admin/folder", "FolderController", ["except" => ["index", "show", "destroy"]]);
  Route::delete("/folders/{id}", "FolderController@destroy")->name("folder.destroy");
});
