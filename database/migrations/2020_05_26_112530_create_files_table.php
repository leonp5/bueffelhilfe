<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('files', function (Blueprint $table) {
      $table->id();
      $table->mediumText("description")->nullable();
      $table->string("username")->nullable();
      $table->integer("userID")->nullable();
      $table->string("folder")->nullable();
      $table->integer("folderID")->nullable();
      $table->string("filename");
      $table->string("pathToDocument");
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('files');
  }
}
