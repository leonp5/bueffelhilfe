<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    User::truncate();
    DB::table("role_user")->truncate();

    $adminRole = Role::where("role", "admin")->first();
    $studentRole = Role::where("role", "student")->first();

    $admin = User::create([
      "name" => "admin",
      "password" => Hash::make("123456")
    ]);

    $student = User::create([
      "name" => "student",
      "password" => Hash::make("123456")
    ]);

    $admin2 = User::create([
      "name" => "admin2",
      "password" => Hash::make("123456")
    ]);

    $admin->roles()->attach($adminRole);
    $student->roles()->attach($studentRole);
    $admin2->roles()->attach($adminRole);
  }
}
