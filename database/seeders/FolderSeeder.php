<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Folder;

class FolderSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Folder::factory()->count(3)->create();
  }
}
