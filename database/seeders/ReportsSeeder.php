<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Report;

// with the number behind "App\Report::class" you can set the number of entries

class ReportsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Report::factory()->count(3)->create();
  }
}
