<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table("content")->insert([
      [
        "title" => "Büffelhilfe. Mathe Nachhilfe in Köln",
        "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus maiores omnis aperiam iusto eligendi sit ipsum obcaecati labore ducimus nam vel at nobis cupiditate libero unde dignissimos velit, dicta rerum corrupti officia vero maxime qui. Voluptate fugiat accusantium sapiente ab natus veritatis, cum recusandae quia non debitis. Debitis, ipsa sint quibusdam ipsum eius quasi expedita veritatis? Iure excepturi, perferendis accusamus officia necessitatibus omnis, eligendi exercitationem deserunt tempore, ea quaerat saepe deleniti distinctio eius accusantium dolorum optio soluta veritatis dolores ex maiores beatae. Et maxime velit architecto praesentium tenetur saepe eligendi minima harum. Magnam, non? Minus expedita quo placeat blanditiis rem!",
        "email" => "wird hier nicht benutzt"
      ],

      [
        "title" => "Lerninhalte",
        "body" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus reiciendis iusto alias tenetur quis neque dolor error possimus laborum fuga. Hic tenetur maxime earum possimus fuga ut natus magnam corporis delectus? Eius quod ea cum vitae dolor. Odio, ad incidunt aut ipsa itaque nisi, vitae sequi numquam veritatis veniam temporibus. Provident molestias doloremque enim, at officia architecto sunt, neque ullam omnis harum ducimus, placeat voluptatem quisquam fuga aut illo? Iusto, tempore nostrum, maiores dignissimos quidem quaerat maxime deleniti ratione rerum laborum provident obcaecati aut soluta. Corrupti voluptate eaque nobis earum soluta aliquam quia perferendis magni tempora! Provident consequuntur quidem dolor!",
        "email" => "wird hier nicht benutzt"
      ],
      [
        "title" => "Über mich",
        "body" => "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad sunt dolorum, incidunt distinctio quos quaerat nam accusamus placeat. Quae, tempora sint. Hic molestias ipsum incidunt pariatur asperiores vitae harum perspiciatis eius tempora voluptatibus soluta vero similique adipisci a saepe iusto nesciunt ut nostrum magni, distinctio sed laudantium nulla impedit? Quia in ab fugit optio sint natus perferendis, id, enim aperiam amet dolores autem eum officia quod inventore minima suscipit quam rem exercitationem quis tempore praesentium ratione voluptas? Temporibus, illum. Iste natus consectetur facilis deserunt enim cum. Tempore architecto ipsam repellendus aut, neque doloribus facere, pariatur cupiditate repudiandae quod quam quidem!",
        "email" => "wird hier nicht benutzt"
      ],
      [
        "title" => "Kontakt",
        "body" => "+123456789",
        "email" => "test@mail.me"
      ],
      [
        "title" => "10 Jahre Erfahrung",
        "body" => "Individuelle Nachhilfe",
        "email" => "Für alle Klassen"
      ]
    ]);
  }
}
