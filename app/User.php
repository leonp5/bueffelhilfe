<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

// bring in for caching current online users

use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   *  // For checking the users last online time
   * 
   * @var array
   */
  protected $casts = [
    "last_online_at" => "datetime",
  ];

  // checking for current online users

  public function isOnline()
  {
    return Cache::has('user-is-online-' . $this->id);
  }


  // This function defines, that this model belongs to the Role model

  public function roles()
  {
    return $this->belongsToMany("App\Role");
  }

  // methods for checking roles of users

  public function roleControl($roles)
  {

    if ($this->roles()->whereIn("role", $roles)->first()) {
      return true;
    }
    return false;
  }


  public function hasRole($role)
  {

    if ($this->roles()->where("role", $role)->first()) {
      return true;
    }
    return false;
  }
}
