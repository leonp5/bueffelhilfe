<?php

namespace App\ActionClasses;

use App\Folder;
use App\User;

class HandleFileUpload
{
  public function execute($request)
  {

    if ($request->input("folderID")) {
      $folderID = $request->input("folderID");
      $folderToSave = "folderID_" . $folderID;
      $folder = Folder::find($folderID);
      $folderName = $folder["name"];
    }

    if ($request->input("userID")) {
      $userID = $request->input("userID");
      $folderToSave = "userID_" . $userID;
      $user = User::find($userID);
      $userName = $user["name"];
    }

    if ($request->hasFile("filename")) {

      // Get filename with the extension
      $documentNameWithExt = $request->file("filename")->getClientOriginalName();
      // Get just filename
      $documentName = pathinfo($documentNameWithExt, PATHINFO_FILENAME);
      // Get just extension
      $extension = $request->file("filename")->getClientOriginalExtension();

      // Insert time and connect filename with extension again for a unique filename

      $documentToStore = $documentName . "_" . time() . "." . $extension;


      $pathToDocument = "files/" . $folderToSave . "/";


      $path = $request->file("filename")->storeAs($pathToDocument, $documentToStore);
    }

    $file = [
      "filename" => isset($documentNameWithExt) ? $documentNameWithExt : null,

      "folder" => isset($folderName) ? $folderName : "File belongs to a user",

      "folderID" => isset($folderID) ? $folderID : null,

      "username" => isset($userName) ? $userName : "File belongs to a folder",

      "userID" => isset($userID) ? $userID : null,

      "pathToDocument" => isset($pathToDocument) ? $pathToDocument . $documentToStore : null

    ];

    return $file;
  }
}
