<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Report extends Model
{
  use HasFactory;

  // protected $table = 'reports';

  protected $fillable = ["name", "body", "visible", "show_in_slider", "rating", "new"];
}
