<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class File extends Model
{
  use HasFactory;

  protected $fillable = ["description", "username", "userID", "folder", "folderID", "filename", "pathToDocument"];
}
