<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    // 'App\Model' => 'App\Policies\ModelPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot()
  {
    $this->registerPolicies();

    // defines the gate for checking if the user is in admin
    // "hasRole" comes from User.php

    Gate::define("administration", function ($user) {
      return $user->hasRole("admin");
    });


    Gate::define("authenticate-users", function ($user) {
      return $user->roleControl(["admin", "student"]);
    });
  }
}
