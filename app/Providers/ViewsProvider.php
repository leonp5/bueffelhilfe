<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Report;

class ViewsProvider extends ServiceProvider
{
  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot()
  {
    // Using Closure based composers

    View::composer('inc.navigationLinks', function ($view) {
      $titles = DB::table("content")->select("title")->get();
      $numberOfNewReports = Report::where("new", 1)->count();

      $view->with(["titles" => $titles, "numberOfNewReports" => $numberOfNewReports]);
    });
  }
}
