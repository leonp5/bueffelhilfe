<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;


// explanation from the tutorial

// 1. First, we check if the user is a guest. If so, we continue on as we can’t set a timestamp for a guest user.

// 2. If the user is authenticated, we retrieve their last online timestamp.

// 3. Next, we determine if this timestamp is more than an hour old (this is a check I added to avoid updating the database on every request, but you could set this check to whatever you want, or remove it entirely).

// 4. Assuming the time difference exceeds one hour, we update the timestamp in the database with the current time.

// 5. Regardless of whether step 4 was performed, we then continue with the user’s request (whatever it might be).


class LastOnlineAt
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if (auth()->guest()) {
      return $next($request);
    }
    if (auth()->user()->last_online_at->diffInHours(now()) !== 0) {
      DB::table("users")
        ->where("id", auth()->user()->id)
        ->update(["last_online_at" => now()]);
    }
    return $next($request);
  }
}
