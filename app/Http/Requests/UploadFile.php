<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadFile extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */

  public function rules()
  {
    return [
      "userID" => 'required_without_all:folderID',
      "folderID" => 'required_without_all:userID',
      'filename' => "required",
    ];
  }

  public function messages()
  {
    return [

      "filename.required" => "Bitte noch eine Datei für den Upload auswählen.",

    ];
  }
}
