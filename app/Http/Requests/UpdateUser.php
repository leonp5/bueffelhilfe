<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {

    return [

      'name' => ["nullable", "sometimes", 'string', 'max:255', "unique:users"],
      'password' => ["nullable", "sometimes", 'string', 'min:6', 'confirmed'],
      'roles' => ['exists:roles,id', "required"],
    ];
  }

  public function messages()
  {
    return [
      'name.unique' => "Diesen Namen gibt's schon.",
      'password.min'  => 'Das Passwort muss mindestens 6 Zeichen haben',
      "roles.required" => "Es müssen Benutzerrechte vergeben werden!"
    ];
  }
}
