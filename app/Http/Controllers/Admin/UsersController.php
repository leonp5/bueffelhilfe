<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Role;
use App\File;
use App\Http\Requests\UpdateUser;

class UsersController extends Controller
{

  public function __construct()
  {
    $this->middleware("auth");
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::all();

    $users->each(function (user $user) {
      $count = File::where("userID", $user->id)->count();
      $user->count = $count;
    });

    return view("admin.users.index")->with("users", $users);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function edit(User $user)
  {
    $roles = Role::all();

    return view("admin.users.edit")->with([
      "user" => $user,
      "roles" => $roles
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateUser $request, User $user)
  {
    $current_user = Auth::User()->id;
    $requested_user = $user->id;

    // check if the current user wants to delete his admin rights

    if ($current_user == $requested_user) {
      $roles = $request->input("roles");

      $isAdmin = "1";

      $exists = in_array($isAdmin, $roles);

      if ($exists == FALSE) {
        $request->session()->flash("error", "Du kannst deine Adminrechte nicht löschen!");
        return redirect("admin/users/$user->id/edit");
      }
    }

    //Save roles
    $user->roles()->sync($request->input("roles"));

    //Save user
    if ($request->input("name")) {
      $user->name = $request->input("name");
    }

    if ($request->input('password')) {
      $user->password = Hash::make($request->input('password'));
    }

    if ($user->save()) {
      $request->session()->flash("success", $user->name . " erfolgreich gespeichert!");
    };
    return redirect()->route("admin.users.index");
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    $current_user = Auth::User()->id;
    $requested_user = $user->id;

    if ($requested_user == $current_user) {
      return redirect()->route("admin.users.index")->with("error", "Du kannst deinen eigenen Account nicht löschen!");
    } else {

      $user->roles()->detach();

      $filesExist = File::all()->contains("userID", $user->id);

      if ($filesExist == true) {
        File::where("userID", $user->id)->delete();

        Storage::disk("local")->deleteDirectory("files/userID_" . $user->id);
      }

      if ($user->delete()) {
        return redirect()->route("admin.users.index")->with("success", "'$user->name'" . " gelöscht!");
      }
    }
  }
}
