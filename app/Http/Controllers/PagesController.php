<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


class PagesController extends Controller
{

  public function main()
  {
    $slides = DB::table('reports')->select('name', 'body', 'rating')->where("show_in_slider", "1")->get();

    $numbered_slides = $slides->map(function ($slide, $number) {
      $slide->number = $number + 1;
      return $slide;
    });
    $content_entries = DB::table("content")->get();

    return view("pages.main")->with(["slides" => $numbered_slides, "content" => $content_entries]);
  }

  public function privacy()
  {
    return view("pages.privacy");
  }

  public function disclaimer()
  {
    return view("pages.disclaimer");
  }

  // public function index()
  // {
  //   $newReports = DB::table("reports")->where("new", "1")->get();

  //   return view('admin.index')->with("reports", $newReports);
  // }

}
