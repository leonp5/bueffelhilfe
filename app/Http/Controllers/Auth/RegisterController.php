<?php

namespace App\Http\Controllers\Auth;

// these two imports are necessary for the custom redirect & auto login disable after registration

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Role;

class RegisterController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

  // this function avoids the auto login after registration

  public function register(Request $request)
  {
    $this->validator($request->all())->validate();

    event(new Registered($user = $this->create($request->all())));

    return $this->registered($request, $user)
      ?: redirect($this->redirectPath());
  }

  use RegistersUsers;


  // use my custom register form for the administrator

  public function showRegistrationForm()
  {
    $roles = Role::all();

    return view('admin.users.add')->with([
      "roles" => $roles
    ]);
  }

  /**
   * Where to redirect admin after creating a new user.
   *
   * @var string
   */
  public function redirectTo()
  {

    $this->redirectTo = route("admin.users.index");

    return $this->redirectTo;
  }

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  // public function __construct()
  // {
  //   $this->middleware('admin');
  // }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'name' => ['required', 'string', 'max:255', "unique:users"],
      'password' => ['required', 'string', 'min:6', 'confirmed'],
      'roles' => ['required', 'exists:roles,id'],
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  protected function create(array $data)
  {

    $user = User::create([
      'name' => $data['name'],
      'password' => Hash::make($data['password']),
    ]);

    // every new user gets the role(s) which is/are checked in the registration form

    $user->roles()->attach($data['roles']);
    session()->flash('success', "Der Benutzer " . "'$user->name'" . " wurde angelegt!");
    return $user;
  }
}
