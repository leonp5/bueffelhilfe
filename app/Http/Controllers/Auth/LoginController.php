<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

// bring in this two because of copying logout function from "AuthenticatesUsers"

use \Illuminate\Http\Response;
use \Illuminate\Http\Request;
use App\Report;

class LoginController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

  // copy this from "AuthenticatesUsers" to attach a logout message

  public function logout(Request $request)
  {
    $this->guard()->logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    if ($response = $this->loggedOut($request)) {
      return $response;
    }
    session()->flash('success', "Du wurdest ausgeloggt!");
    return $request->wantsJson()
      ? new Response('', 204)
      : redirect('/');
  }


  use AuthenticatesUsers;

  /**
   * Where to redirect users after login.
   *
   * @var string
   */

  // commenting out this line and add a specific redirectTo function for a custom redirect

  // protected $redirectTo = RouteServiceProvider::HOME;

  // needs to bring in Auth Facade

  public function redirectTo()
  {
    $user = Auth::user()->name;
    if (Auth::user()->hasRole("admin")) {
      $thereAreNewReports = Report::all()->contains("new", 1);
      if ($thereAreNewReports == true) {
        $this->redirectTo = route("reports");
      } else {
        $this->redirectTo = route("files.index");
      }
    } elseif (Auth::user()->hasRole("student")) {
      $this->redirectTo = route("files.index");
    } else {
      $this->redirectTo = route("main");
    }



    session()->flash('success', "Hallo " . $user . "!" . ' Du bist eingeloggt!');

    return $this->redirectTo;
  }

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  // define, that the user can login with "name"

  public function username()
  {
    return "name";
  }
}
