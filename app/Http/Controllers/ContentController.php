<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Http\Requests\UpdateContent;

class ContentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Content  $content
   * @return \Illuminate\Http\Response
   */
  public function show(Content $content)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Content  $content
   * @return \Illuminate\Http\Response
   */
  public function edit(Content $content)
  {
    return view("pages.inc.edit")->with("content", $content);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Content  $content
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateContent $request, $id)
  {

    $content = Content::find($id);

    $content->title = $request->input("title");
    $content->body = $request->input("body");

    if ($request->input("email")) {
      $content->email = $request->input("email");
    }

    if ($content->save()) {
      return redirect()->route("main")->with("success", "Änderungen gespeichert!");
    } else {
      $request->session()->flash("error",  "Das hat nicht geklappt!");
      return back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Content  $content
   * @return \Illuminate\Http\Response
   */
  public function destroy(Content $content)
  {
    //
  }
}
