<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;


use App\File;
use App\Folder;
use App\User;
use App\Http\Requests\UpdateFile;
use App\Http\Requests\UploadFile;
use App\ActionClasses\HandleFileUpload;


class FileController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $allFolders = Folder::all()->sortBy("name");
    $userID = Auth::User()->id;
    if (File::all()->contains("userID", $userID)) {
      $personalFolderExists = true;
    } else {
      $personalFolderExists = false;
    }

    // count how many files per folder exist

    $allFolders->each(function (folder $folder) {
      $count = File::where("folderID", $folder->id)->count();
      $folder->count = $count;
    });

    if (Gate::allows("administration")) {

      return view("pages.files.index")->with(["folders" => $allFolders, "personalFolder" => $personalFolderExists]);
    } else {
      $visibleFolders = $allFolders->filter(function ($value) {
        return $value["visible"];
      });
      return view("pages.files.index")->with(["folders" => $visibleFolders, "personalFolder" => $personalFolderExists]);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $folders = Folder::all(["id", "name"])->sortBy("name");

    $users = User::all(["id", "name"])->sortBy("name");

    return view("pages.files.addFile")->with(["folders" => $folders, "users" => $users]);
  }

  public function addFileToFolder($folderID)
  {
    $folder = Folder::find($folderID, ["id", "name"]);
    return view("pages.files.addFile")->with("folder", $folder);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(UploadFile $request, HandleFileUpload $action)
  {

    $savedFile = $action->execute($request);

    $file = new File([

      "filename" => $savedFile["filename"],

      'description' => $request->get('description'),

      "username" => $savedFile["username"],

      "userID" => $savedFile["userID"],

      "folder" => $savedFile["folder"],

      "folderID" => $savedFile["folderID"],

      "pathToDocument" =>  $savedFile["pathToDocument"]

    ]);

    if ($file->save()) {
      $saved = true;
    }

    if ($saved == true) {
      if ($savedFile["folderID"]) {
        return redirect()->route("files.show", [$savedFile["folderID"]])->with("success", "Datei hochgeladen!");
      }

      if ($savedFile["userID"]) {

        $current_user = Auth::User()->id;
        if ($savedFile["userID"] == $current_user) {
          return redirect()->route("personalFiles.show")->with("success", "Datei hochgeladen!");
        } else {
          return redirect()->route("admin.userFiles.show", $savedFile["userID"])->with("success", "Datei hochgeladen!");
        }
      }
    } else {
      return back()->with("error", "Das hat nicht geklappt!");
    }
  }



  public function downloadSingleFile($id)
  {

    $document = File::where('id', $id)->first();

    if ($document) {
      $file = storage_path("app/" . $document->pathToDocument);
      $name = $document->filename;
      $headers = [
        'Content-Disposition' => 'attachment',
        'Cache-Control' => 'no-cache, no-store, must-revalidate',
        'Pragma' => 'no-cache',
        'Expires' => '0',
      ];
      return Response::download($file, $name, $headers);
    } else {
      return abort(404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\File  $files
   * @return \Illuminate\Http\Response
   */
  public function showCommonFiles($folderID)
  {

    $files = File::all()->where("folderID", $folderID)->sortBy("title");
    $folderList = Folder::all(["id", "name"])->sortBy("name");

    if (count($files) < 1) {
      return back()->with("error", "Dieser Ordner ist leer!");
    }

    // generate heading for the page

    $folder = Folder::find($folderID);
    $heading = $folder->name;

    if ($files) {
      return view("pages.files.showFiles")->with(["files" => $files, "heading" => $heading, "folders" => $folderList]);
    }
  }

  public function showPersonalFiles()

  {
    $current_userID =  Auth::User()->id;
    $folderList = Folder::all(["id", "name"])->sortBy("name");

    $files = File::all()->where("userID", $current_userID)->sortByDesc("created_at");

    return view("pages.files.showFiles")->with(["files" => $files, "heading" => "Deine Dateien", "folders" => $folderList]);
  }

  public function showUserFiles($userID)
  {
    $userName = User::find($userID)->name;
    $folderList = Folder::all(["id", "name"])->sortBy("name");

    $files = File::all()->where("userID", $userID);

    return view("pages.files.showFiles")->with(["files" => $files, "heading" => "Alle Dateien von " . "'$userName'", "folders" => $folderList]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\File  $files
   * @return \Illuminate\Http\Response
   */
  public function edit($fileID)
  {
    $file = File::find($fileID);

    $folders = Folder::all(["id", "name"]);

    $users = User::all(["id", "name"]);

    return view("pages.files.editFile")->with(["file" => $file, "users" => $users, "folders" => $folders]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\File  $files
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateFile $request, $id, HandleFileUpload $action)
  {
    $file = File::find($id);

    $savedFile = $action->execute($request);

    if ($request->hasFile("filename")) {
      Storage::disk("local")->delete($file->pathToDocument);
    }

    $file->description = $request->input("description");

    $file->username = $savedFile["username"];

    $file->userID = $savedFile["userID"];

    $file->folder = $savedFile["folder"];

    $file->folderID = $savedFile["folderID"];

    $file->filename = isset($savedFile["filename"]) ? $savedFile["filename"] : $file->filename;

    $file->pathToDocument = isset($savedFile["pathToDocument"]) ? $savedFile["pathToDocument"] : $file->pathToDocument;

    if ($file->save()) {
      $saved = true;
    }

    if ($saved == true) {
      if ($savedFile["folderID"]) {
        return redirect()->route("files.show", [$savedFile["folderID"]])->with("success", "Änderungen gespeichert!");
      }
      if ($savedFile["userID"]) {

        $current_user = Auth::User()->id;
        if ($savedFile["userID"] == $current_user) {
          return redirect()->route("personalFiles.show")->with("success", "Änderungen gespeichert!");
        } else {
          return redirect()->route("admin.userFiles.show", $savedFile["userID"])->with("success", "Änderungen gespeichert!");
        }
      }
    } else {
      return back()->with("error", "Das hat nicht geklappt!");
    }
  }

  public function filesDeleteOrMove(Request $request)
  {


    $allFiles = $request->input("files");
    $files = collect($allFiles);
    $selectedFiles = $files->where("selected", 1);

    if (count($selectedFiles) > 1) {
      $singularOrPlural = "Dateien ";
    } else {
      $singularOrPlural = "Datei ";
    }

    $newFolderID = $request->input("folderID");

    foreach ($selectedFiles as $file) {
      $file = File::find($file["id"]);

      $folderID = isset($file->folderID) ? $file->folderID : null;

      $userID = isset($file->userID) ? $file->userID : null;

      if ($newFolderID == null) {

        Storage::disk("local")->delete($file->pathToDocument);

        $file->delete();

        $message = $singularOrPlural . "gelöscht!";
      } else {

        $folderToSave = "folderID_" . $newFolderID;

        $oldPath = storage_path("app/" . $file->pathToDocument);

        // get Filename by cutting the string after the last /
        $fileToMove = substr($oldPath, strrpos($oldPath, '/') + 1);

        $newPath = "files/" . $folderToSave . "/" . $fileToMove;

        Storage::move($file->pathToDocument, $newPath);

        $file->pathToDocument = $newPath;
        $file->folderID = $newFolderID;
        $file->save();

        $message = $singularOrPlural . "verschoben!";
      }
    }

    // delete folder, if its empty

    if ($folderID) {

      $filesExist = File::all()->contains("folderID", $folderID);

      if ($filesExist == false) {
        Storage::disk("local")->deleteDirectory("files/folderID_" . $folderID);
        return redirect()->route("files.index")->with("success", $message);
      }
      return back()->with("success", $message);
    } elseif ($userID) {

      $filesExist = File::all()->contains("userID", $userID);

      if ($filesExist == false) {
        Storage::disk("local")->deleteDirectory("files/userID_" . $userID);
      }
      return back()->with("success", $message);
    } else {
      return back()->with("error",  "Das hat nicht geklappt!");
    }
  }
}
