<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Folder;
use App\File;

class FolderController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view("pages.folders.addFolder");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $folder = new Folder([

      "name" => $request->input("name"),

      "description" => $request->input("description"),

      "visible" => $request->input("visible")
    ]);

    if ($folder->save()) {
      $saved = true;
    }
    if ($saved == true) {
      return redirect()->route("files.index")->with("success", "Neuen Ordner angelegt!");
    } else {
      return back()->with("error", "Das hat nicht geklappt!");
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($folderID)
  {
    $folder = Folder::find($folderID);

    return view("pages.folders.editFolder")->with("folder", $folder);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Folder $folder)
  {
    $folder->update(request()->validate([
      "name" => "required",
      "description" => "nullable",
      "visible" => "required"
    ]));

    return redirect()->route("files.index")->with("success", "Änderungen gespeichert!");
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    File::where("folderID", $id)->delete();
    Folder::find($id)->delete();
    Storage::disk("local")->deleteDirectory("files/folderID_" . $id);

    return back()->with("success", "Ordner gelöscht!");
  }
}
