<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Report;
use App\Http\Requests\ReportUpdate;

class ReportsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $allReports = Report::all();
    $newReports = Report::all()->where("new", 1);

    // manage the visibility and "statistics" of the reports

    $visibleReports = $allReports->filter(function ($value) {
      return $value["visible"];
    });

    $visibleReportsCounted = $visibleReports->count();
    $visibleReportsSum = $visibleReports->sum("rating");
    if ($visibleReportsSum == 0) {
      $visibleReportsAverage = 0;
    } else {
      $visibleReportsAverage = round((float) $visibleReportsSum / $visibleReportsCounted, 1);
    }

    $visibleReportsStats = ["countedReports" => $visibleReportsCounted, "average" => $visibleReportsAverage];


    if (Gate::allows('administration')) {
      $countedReports = $allReports->count();
      $sum = $allReports->sum("rating");
      $average = round((float) $sum / $countedReports, 1);

      $reportsStats = ["countedReports" => $countedReports, "average" => $average, "visibleReports" => $visibleReportsCounted, "visibleReportsAverage" => $visibleReportsAverage];
      return view("pages.reports.index")->with(["reports" => $allReports, "reportsStats" => $reportsStats, "newReports" => $newReports]);
    } else {

      return view("pages.reports.index")->with(["reports" => $visibleReports, "reportsStats" => $visibleReportsStats]);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $report = new Report([

      'name' => $request->get('name'),

      'body' => $request->get('body'),

      'visible' => $request->get('visible'),

      'show_in_slider' => $request->get('show_in_slider'),

      "rating" => $request->get("rating"),

      'new' => $request->get('new')

    ]);

    if ($report->save()) {

      return response()->json(['success' => true, "message" => "Feedback gespeichert!"], 200);
    } else {
      return response()->json(['success' => false, "message" => "Das hat nicht geklappt!"], 500);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Report $report)
  {
    return view("pages.reports.editReport")->with("report", $report);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(ReportUpdate $request, $id)
  {
    $report = Report::find($id);

    if ($request->input("name")) {
      $report->name = $request->input("name");
    }

    if ($request->input("body")) {
      $report->body = $request->input("body");
    }

    // Update Report checkboxes

    if ($request->input("visible") == null) {
      $report->visible = "0";
    } else {
      $report->visible = "1";
    }
    if ($request->input("show_in_slider") == null) {
      $report->show_in_slider = "0";
    } else {
      $report->show_in_slider = "1";
    }

    $report->rating = $request->input("rating");

    // remove "new" if exists

    if ($request->input("new") == null) {
      $report->new = false;
    }


    if ($report->save()) {
      $request->session()->flash("success",  "Änderungen gespeichert!");
    } else {
      $request->session()->flash("error",  "Das hat nicht geklappt!");
    }


    if ($request->input("body") == true || $request->input("name") == true) {
      return redirect()->route("admin.reports.index");
    } else {

      return back();
    }
  }

  public function MultipleUpate(ReportUpdate $request)
  {
    $reports = $request->input("reports");

    foreach ($reports as $row) {
      $report = Report::find($row["id"]);

      // giving the checkbox 0, if it isn't checked

      $isVisible = isset($row["visible"]) ? 1 : 0;
      $inSlider = isset($row["show_in_slider"]) ? 1 : 0;

      $report->visible = $isVisible;
      $report->show_in_slider = $inSlider;

      $report->new = false;

      if ($report->save()) {
        $saved = true;
      }
    }

    if ($saved == true) {
      $request->session()->flash("success",  "Änderungen gespeichert!");
    } else {
      $request->session()->flash("error",  "Das hat nicht geklappt!");
    }

    return back();
  }

  public function CheckBoxUpdate(ReportUpdate $request)
  {

    $report = Report::find($request->id);


    // Update Report checkboxes

    if ($request->input("visible") == null) {
      $report->visible = "0";
    } else {
      $report->visible = "1";
    }
    if ($request->input("show_in_slider") == null) {
      $report->show_in_slider = "0";
    } else {
      $report->show_in_slider = "1";
    }

    $report->new = false;

    if ($report->save()) {
      $saved = true;
    }

    if ($saved == true) {
      $request->session()->flash("success",  "Änderungen gespeichert!");
    } else {
      $request->session()->flash("error",  "Das hat nicht geklappt!");
    }

    return back();
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Report $report)
  {
    if ($report->delete()) {

      return back()->with("success",  "Feedback von " . "'$report->name'" . " gelöscht!");
    }
  }
}
