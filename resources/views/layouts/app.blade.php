<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Büffelhilfe') }}</title>

  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

  @yield('head')

</head>

<body>
  <div class="main-container" id="main-container">
    @include("inc.header")
    @include('auth.loginContainer')
    @include('auth.loginModal')
    @include("inc.mobileNavigation")
      @yield("headerimage")
      <div class="background-container">
      <div class="content-container" id="content">
          @include("inc.alerts")
          @yield ("content")

      </div>
    </div>
    @include('inc.footer')
    @include("inc.jsHint")
  </div>

  <!-- Scripts -->

  <script src="{{ asset('js/app.js')}}"></script>


  @stack('scripts')

</body>

</html>
