<h4>Benutzerrechte:</h4>
<div class="row-container">
  @foreach ($roles as $role)

  @if($user ?? '')

  <div class="checkbox-container">
    <input class="checkbox" type="checkbox" name="roles[]" value="{{ $role->id}}"
      @if($user->roles->pluck('id')->contains($role->id))
    checked
    @endif>
    <span class="checkmark"></span>
  </div>
  @else
  <div class="checkbox-container">
    <input class="checkbox" type="checkbox" name="roles[]" value="{{ $role->id}}">
    <span class="checkmark"></span>
  </div>
  @endif

  {{Form::label("body", $role->role)}}
  @endforeach
</div>
<div class="row-grid">
  @if($user ?? '')
  <h4>Login Name ändern:</h4>
  @else
  <h4>Login Name:</h4>
  @endif
  {{Form::text("name", "",  ["class" => "edit-input", "placeholder" => "Neuer Name..."])}}
</div>
<div class="row-grid">
  <h4>Neues Passwort:</h4>
  {{Form::password("password", ["class" => "edit-input", "placeholder" => "Neues Passwort", "id" => "password", "name" => "password"])}}

</div>
<div class="row-grid">
  <h4>Passwort wiederholen:</h4>
  {{Form::password("password_confirmation", ["class" => "edit-input", "placeholder" => "Passwort wiederholen", 'id' => 'password-confirm'])}}
</div>