@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>User bearbeiten</h2>

{!! Form::open(["route" => ["admin.users.update", $user], "method" => "POST", "class" => "form-container"]) !!}


<h3>"{{$user->name}}" bearbeiten</h3>

@include("admin.users.inc.userForm")

{{Form::hidden("_method", "PUT")}}
<div class="row-space-between margin-30 top"><a class="abort-btn" href="{{route("admin.users.index")}}">Abbrechen</a>
  <button class="action-btn">Speichern</button>
</div>
{!! Form::close() !!}


@endsection