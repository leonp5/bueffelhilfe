@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>Benutzerverwaltung</h2>

<div class="row">
  <a class="action-btn" href="{{route("admin.user.create")}}">Benutzer hinzufügen</a>
</div>


<table class="table">
  <thead>
    <tr>
      <th class="th">Name</th>
      <th class="th">Status</th>
      <th class="th">Zuletzt online</th>
      <th class="th">Rechte</th>
      <th class="th">Aktionen</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($users as $user)
    <tr class="tr">

      <td class="td"><a class="table-link-color" href={{route("admin.userFiles.show", $user->id)}}>{{$user->name}}
          ({{$user->count}})</a>
      </td>

      @if ($user->isOnline())
      <td class="td-online">online</td>
      @else
      <td class="td-offline">offline</td>
      @endif

      <td class="td">{{$user->last_online_at->translatedFormat("D, j.m.y, H:i")}} Uhr</td>
      <td class="td">{{ implode(", ", $user->roles()->get()->pluck("role")->toArray()) }}</td>

      <td class="td">
        <div class="td-actions">
          <a class="table-link" href={{route("admin.users.edit", $user->id)}}>
            <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit"/></a>
          <button class="table-button" onclick="ToggleDeleteModal({{$user}})" id="toggleDeleteIsNecessary">
            <x-svg svg="trash" width=36 height=36 viewBox="24 24" class="delete"/>
          </button>
        </div>
      </td>


    </tr>

    @endforeach

  </tbody>
</table>
@endsection