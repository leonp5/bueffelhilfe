@extends('layouts.app')


@section('content')

@include("inc.backButton")

<h2>Benutzer Erstellen</h2>
<div class="form-container">
  {!! Form::open(["route" => "admin.user.create", "method" => "POST"]) !!}

  <h3>Neuen Benutzer anlegen</h3>

  @include('admin.users.inc.userForm')

  <div class="row-space-between margin-30 top"><a class="abort-btn" href="{{route("admin.users.index")}}">Abbrechen</a>
    <button class="action-btn" type="submit">Nutzer anlegen</button>
  </div>
  {!! Form::close() !!}
</div>


@endsection