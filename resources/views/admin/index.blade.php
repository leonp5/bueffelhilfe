{{-- Not used anymore. Maybe later again? --}}

@extends('layouts.app')

@section('content')
<h2>Verwaltung</h2>
<a class="floating-add-file" href="{{route("admin.file.create")}}">
  <x-svg svg="file" fill="#000000" viewBox="24 24" class="file-svg"/>
</a>
<div class="admin-nav">
  <a class="admin-nav-link" href="{{route("admin.users.index")}}">Benutzerverwaltung</a>

  <a class="admin-nav-link" href="{{route("admin.user.create")}}">Benutzer hinzufügen</a>

  <a class="admin-nav-link" href="{{route("admin.reports.index")}}">Feedbackverwaltung</a>

  <a class="admin-nav-link" href="{{route("files.index")}}">Dateiverwaltung</a>
</div>


@if(count($newReports) > 0)

<h2>Neue Feedback Einträge</h2>

{!! Form::open(["route" => ["admin.reports.multiupdate"], "method" => "PUT", "class" => "report-table"]) !!}
<div class="th-row">
  <div class="th-author">Name</div>
  <div class="th-flex">Feedback</div>
  <div class="th-wrapper">
    <div class="th-visible">
      <p class="th-text">Sichtbar</p>
    </div>
    <div class="th-slider">
      <p class="th-text">Im Slider</p>
    </div>
    <div class="th-small">Bearbeiten</div>
  </div>
</div>


@foreach ($newReports as $newReport)
<div class="report-tr">
  <input type="hidden" name="reports[{{$loop->index}}][id]" value="{{$newReport->id}}">
  <div class="td-name">
    <p class="td-text">{{$newReport->name}}</p>
    <div class="show-star-rating-sm" style="--rating: {{$newReport->rating}}">
    </div>
    <div class="show-star-rating-l" style="--rating: {{$newReport->rating}}">
    </div>
  </div>
  <div class="td-flex">{{$newReport->body}}</div>
  <div class="tr-wrapper">
    <div class="form-wrapper">
      <div class="checkbox-wrapper">
        <div class="checkbox-visible">
          <div class="checkbox-container">
            <input class="checkbox" type="checkbox" name="reports[{{$loop->index}}][visible]" value="1" checked>
            <span class="checkmark"></span>
          </div>
          <label class="table-label" for="visible">Sichtbar</label>
        </div>
        <div class="checkbox-slider">
          <div class="checkbox-container">
            <input class="checkbox" type="checkbox" name="reports[{{$loop->index}}][show_in_slider]" value="1"
              {{($newReport->show_in_slider == 1 ? "checked" : "")}}>
            <span class="checkmark"></span>
          </div>
          <label class="table-label" for="show_in_slider">Im Slider</label>
        </div>

      </div>
    </div>

    <a class="table-link table-edit" href={{route("admin.reports.edit", $newReport->id)}}>
      <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="table-svg" />
    </a>

  </div>
</div>
@endforeach
<button class="floating-save">
  <x-svg svg="saveAll" id="path90" height="36" width="36" viewBox="32 32" class="saveAll" />
</button>
{!! Form::close() !!}

@endif

@endsection