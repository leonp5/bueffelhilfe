@extends('layouts.app')

@section('content')

<h1>Impressum</h1>

<h2>Technische Umsetzung</h2>
<p class="lh-m">Leon Pelzer<br>
  E-Mail:
  <a class="basic-link" href="mailto:leonpe@web.de">leonpe@web.de</a><br>
  GitLab Profil:
  <a class="basic-link" href="https://gitlab.com/leonp5" target="_blank">Hier entlang</a>
</p>

<h2>Angaben gem&auml;&szlig; &sect; 5 TMG</h2>
<p class="lh-m">David Menrath<br />
  Senefelderstra&szlig;e 16<br />
  50825 K&ouml;ln</p>

<h2>Kontakt</h2>
{{-- <p>Telefon: {{ env('HANDY_NR') }}<br /> --}}
<p>
  E-Mail: <a class="basic-link" href="mailto:david.menrath@gmail.com">david.menrath@gmail.com</a></p>

{{-- <h2>Verbraucher&shy;streit&shy;beilegung/Universal&shy;schlichtungs&shy;stelle</h2>
<p>Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle
  teilzunehmen.</p> --}}

<h3>Haftung f&uuml;r Inhalte</h3>
<p>Als Diensteanbieter bin ich gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten nach den
  allgemeinen Gesetzen verantwortlich. Nach &sect;&sect; 8 bis 10 TMG bin ich als Diensteanbieter jedoch nicht
  verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu &uuml;berwachen oder nach Umst&auml;nden zu
  forschen, die auf eine rechtswidrige T&auml;tigkeit hinweisen.</p>
<p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben
  hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten
  Rechtsverletzung m&ouml;glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werde ich diese Inhalte
  umgehend entfernen.</p>
<h3>Haftung f&uuml;r Links</h3>
<p>Mein Angebot enth&auml;lt Links zu externen Websites Dritter, auf deren Inhalte ich keinen Einfluss habe. Deshalb
  kann ich f&uuml;r diese fremden Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der
  verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten
  wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige
  Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p>
<p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer
  Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werde ich derartige Links umgehend
  entfernen.</p>
<h3>Urheberrecht</h3>
<p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht.
  Die Vervielf&auml;ltigung, Bearbeitung, Verbreitung und jede Art der Verwertung au&szlig;erhalb der Grenzen des
  Urheberrechtes bed&uuml;rfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien
  dieser Seite sind nur f&uuml;r den privaten, nicht kommerziellen Gebrauch gestattet.</p>
<p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet.
  Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung
  aufmerksam werden, bitte ich um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werde ich
  derartige Inhalte umgehend entfernen.</p>

<p>Quelle: <a class="basic-link" href="https://www.e-recht24.de">eRecht24</a></p>

<div class="row-wrap">
  <div class="column margin-50">
    <h3>Fotos</h3>
    <p class="center-text">Header Bild auf der Startseite von <a class="basic-link"
        href="https://pixabay.com/de/users/Pixapopz-2873171/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1547018"
        target="_blank">Chuk
        Yong</a> auf <a class="basic-link"
        href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1547018"
        target="_blank">Pixabay</a>
    </p>
  </div>
  <div class="column margin-50">
    <h3>Icons & Grafiken</h3>
    <p class="center-text">"Tafel mit Apfel" von <a class="basic-link"
        href="https://pixabay.com/de/users/Clker-Free-Vector-Images-3736/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=23421"
        target="_blank">Clker-Free-Vector-Images</a>
      auf <a class="basic-link"
        href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=23421"
        target="_blank">Pixabay</a>
    </p>
    <p>
      WhatsApp Icon: <a class="basic-link" href="https://de.wikipedia.org/wiki/Datei:WhatsApp.svg"
        target="_blank">Wikipedia</a>
    </p>
    <p>
      Telegram Icon: <a class="basic-link" href="https://de.wikipedia.org/wiki/Datei:Telegram_logo.svg"
        target="_blank">Wikipedia</a>
    </p>

    <p class="center-text">
      <x-svg svg="folder" fill="#000000" width=26 height=26 viewBox="26 26" class="disclaimer-svg" />
      und
      <x-svg svg="file" fill="#000000" viewBox="24 24" class="disclaimer-svg" />
      Icon
      von
      <a class="basic-link" href="https://icons8.com" target="_blank">
        Icons8</a> </p>
    <p class="center-text">
      <x-svg svg="save" width=36 height=36 viewBox="512 512" class="disclaimer-svg" />

      Icon von
      <a class="basic-link" href="https://www.flaticon.com/authors/freepik" target="_blank">
        Freepik </a> auf <a class="basic-link" target="_blank" href="https://www.flaticon.com/">Flaticon</a></p>

    <p class="center-text">
      <x-svg svg="call" width=36 height=36 viewBox="512.076 512.076" class="disclaimer-svg" />
      und
      <x-svg svg="email" width=36 height=36 viewBox="511.974 511.974" class="disclaimer-svg" />
      Icon von
      <a class="basic-link" href="https://www.flaticon.com/authors/smashicons" target="_blank">
        Smashicons</a> auf <a class="basic-link" target="_blank" href="https://www.flaticon.com/">Flaticon</a></p>

    <p class="center-text">
      <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="disclaimer-svg" />
      und
      <x-svg svg="trash" fill="none" width=36 height=36 viewBox="24 24" class="disclaimer-svg" />
      von <a class="basic-link" target="_blank" href="https://twitter.com/steveschoger">
        Steve Shoger</a> und <a class="basic-link" href="https://twitter.com/adamwathan" target="_blank">Adam Wathan</a>
      auf
      <a class="basic-link" href="https://heroicons.dev/" target="_blank">
        heroicons.dev</a> </p>

    <p class="center-text">
      <x-svg svg="plus" viewBox="469.33333 469.33333" class="disclaimer-svg" />
      von
      <a class="basic-link" href="https://www.flaticon.com/authors/pixel-perfect" target="_blank">
        Pixel perfect</a> auf <a class="basic-link" target="_blank" href="https://www.flaticon.com">Flaticon</a> </p>

    <p class="center-text">
      <x-svg svg="saveAll" id="path90" height="36" width="36" viewBox="32 32" class="disclaimer-svg" />
      von
      <a class="basic-link" href="https://commons.wikimedia.org/wiki/File:Breezeicons-actions-32-document-save-all.svg"
        target="_blank">
        Wikipedia</a> </p>

    <p class="center-text">
      <x-svg svg="copy" fill="none" width=36 height=36 viewBox="368.008 368.008" class="copy" />
      von <a class="basic-link" href="https://www.flaticon.com/authors/vitaly-gorbachev" target="_blank">Vitaly
        Gorbachev</a> von <a class="basic-link" href="https://www.flaticon.com" target="_blank">Flaticon</a>
    </p>
  </div>

</div>


@endsection