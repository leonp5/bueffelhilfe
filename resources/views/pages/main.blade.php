@extends('layouts.app')

@section('headerimage')

@if($content ?? "")

<div class="img-container">

  <div class="main-img-text-container">
    @can('administration')
    <div class="align-right"><a class="table-button" href="{{route("admin.content.edit", $content[4]->id)}}">
        <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit-active" />
      </a>
    </div>
    @endcan

    <h2 class="badge-heading">{{$content[4]->title}}</h2>
    <h2 class="badge-heading">{{$content[4]->body}}</h2>
    <h2 class="badge-heading">{{$content[4]->email}}</h2>
  </div>
  <div class="slider-container mq-max-m" id="desktop-slider">
    @include('pages.inc.slider')
  </div>

  <img class="header-image" src="{{asset("storage/images/home_1400.jpg")}}" alt="Mathe Nachhilfe Köln">
  <div class="img-overlay-white"></div>
  <div class="img-overlay-transparent"></div>
</div>
@else

<p>Es gibt keinen Content in der Datenbank!</p>

@endif

@endsection

{{-- put the modal here, because inside of the content section it only takes the half of the screen and i don't know why --}}

@include('inc.imageModal')

@section('content')

@if($content ?? "")

<div class="slider-container mq-min-m" id="mobile-slider">
  @include('pages.inc.slider')
</div>

@can('administration')

<div class="align-right"><a class="table-button" href="{{route("admin.content.edit", $content[0]->id)}}">
    <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit-active" />
  </a>
</div>

@endcan

<h2 class="h2" id="start">{{$content[0]->title}}</h2>
<div class="svg-text-wrapper">
  <x-svg svg="blackboard" width=100 height=100 viewBox="575 700" class="blackboard-svg float-left" />
  <p>
    {{$content[0]->body}}
  </p>
</div>
@can('administration')

<div class="align-right"><a class="table-button" href="{{route("admin.content.edit", $content[1]->id)}}">
    <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit-active" />
  </a>
</div>

@endcan

<h2 class="h2" id="lerninhalte">{{$content[1]->title}}</h2>
<p class="desktop-flex-reverse">
  <img class="content-img-right float-right" src="{{asset("storage/images/mathe-endlich_250.jpg")}}">
  {{$content[1]->body}}
</p>

@can('administration')

<div class="align-right"><a class="table-button" href="{{route("admin.content.edit", $content[2]->id)}}">
    <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit-active" />
  </a>
</div>

@endcan

<h2 class="h2" id="uebermich">{{$content[2]->title}}</h2>

<p class="desktop-flex">
  <a onClick="ToggleImage()" class="toggle-img-btn">
    <img class="content-img-left float-left" src="{{asset("storage/images/david_menrath_bueffelhilfe_170.jpg")}}"
      alt="foto_david_menrath" /></a>
  {{$content[2]->body}}</p>
@can('administration')

<div class="align-right"><a class="table-button" href="{{route("admin.content.edit", $content[3]->id)}}">
    <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit-active" />
  </a>
</div>

@endcan

<h2 class="h2" id="kontakt">{{$content[3]->title}}</h2>
<div class="contact-box">
  <div class="contact-row"><a class="invisible-anchor" href="{{ env('WA_LINK') }}" target="_blank" rel="noopener">
      <img class="wa-img" src="{{asset("storage/images/WhatsApp.png")}}">
      <a class="invisible-anchor" href="{{ env('TELEGRAM_LINK') }}" target="_blank" rel="noopener">
        <img class="telegram-img" src="{{asset("storage/images/Telegram.png")}}">
      </a>
    </a></div>
  <div class="contact-row">
    <x-svg svg="call" width=36 height=36 viewBox="512.076 512.076" class="contact-svg" />
    <p class="contact-text">{{$content[3]->body}}</p>
  </div>
  <div class="contact-row">
    <x-svg svg="email" width=36 height=36 viewBox="511.974 511.974" class="contact-svg" />
    <p class="contact-text"><a class="basic-link" href="mailto:{{$content[3]->email}}">{{$content[3]->email}}</a></p>
  </div>
</div>

@else

<p>Es gibt keinen Content in der Datenbank!</p>

@endif

@include('inc.contactMap')

@endsection