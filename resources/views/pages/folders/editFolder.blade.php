@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>"{{$folder->name}}" bearbeiten</h2>

{!! Form::open(["route" => ["admin.folder.update", $folder], "class" => "form-container", "method" => "PUT"]) !!}

@include("pages.folders.inc.folderForm")

<div class="row-space-between margin-30 top"><a class="abort-btn" href="{{url()->previous()}}">Abbrechen</a>
  <button class="action-btn" type="submit">Änderungen speichern</button>
</div>

{!! Form::close() !!}

@endsection