{{Form::label("name", "Name", ["class" => "input-label"])}}

{{Form::text("name", $folder->name ?? "", ["class" => "report-input", "id" => "folderNameCheckNecessary", "oninput" => "ValidateFolderName()"])}}

{{Form::label("description", "Beschreibung", ["class" => "input-label"])}}

{{Form::textarea("description", $folder->description ?? "", ["class" => "textarea"])}}
<div class="small-row margin-20 top">
  {{Form::label("visible", "Für Schüler sichtbar", ["class" => "input-label"])}}

  <div class="checkbox-container">
    <input type="hidden" name="visible" value="0">
    @if($folder ?? "" )
    <input class="checkbox" type="checkbox" name="visible" value="1" {{(  $folder->visible == 1 ? "checked" : "")}}>
    @else
    <input class="checkbox" type="checkbox" name="visible" value="1" checked>
    @endif
    <span class="checkmark"></span>
  </div>
</div>