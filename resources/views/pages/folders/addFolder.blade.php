@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>Neuen Ordner anlegen</h2>

{!! Form::open(["route" => "admin.folder.store", "class" => "form-container", "method" => "POST"]) !!}

@include("pages.folders.inc.folderForm")

<div class="row-space-between margin-30 top"><a class="abort-btn" href="{{url()->previous()}}">Abbrechen</a>
  <button class="action-btn" type="submit">Ordner anlegen</button>
</div>

{!! Form::close() !!}

@endsection