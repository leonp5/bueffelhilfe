@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>{{$heading}}</h2>

@can("administration")
{{Form::open(["route" => ["admin.filesDeleteOrMove"], "method" => "DELETE","class" => "container-100 column", "id" => "editAndDeleteForm"])}}

<a class="floating-add-file" href="{{route("admin.file.create")}}">
  <x-svg svg="file" fill="#000000" viewBox="24 24" class="file-svg"/>
</a>
<button class="floating-delete" id="floating-delete">
  <x-svg svg="trash" fill="none" width=36 height=36 viewBox="24 24"/>
</button>

{{-- Enable move button only in folders for common files --}}
@if($files->first()->folderID ?? "")
<a class="floating-move" onclick="ToggleMoveModal()" id="floating-move">
  <x-svg svg="copy" fill="none" width=36 height=36 viewBox="368.008 368.008" class="copy"/>
</a>
@endif

@include("pages.files.inc.fileMove")

@endcan

@if(count($files) > 0)
@can("administration")
<div class="select-all" id="toggleSelectAllIsNecessary">
  <p class="medium margin-10 right"> Alle auswählen</p>
  <div class="margin-20">
    <div class="checkbox-container">
      <input class="checkbox" type="checkbox" onclick="ToggleSelectAll()">
      <span class="checkmark"></span>
    </div>
  </div>
</div>
@endcan
@foreach ($files as $file)
<div class="file-container" id={{$file->id}}>

  @can("administration")

  <input type="hidden" name="files[{{$loop->index}}][id]" value="{{$file->id}}">

  @endcan

  <div class="file-title-box" id="ShowAndHideButtonsNecessary">
    <div class="file-wrapper">
      <div class="small-row clickable" onclick="ToggleDescriptionBox({{$file->id}})">
        <span class="arrow-right-l" id="arrow{{$file->id}}">
          <span></span>
          <span></span>
        </span>
      </div>
      <a class="file file-svg-hover" href={{route("file.get", $file->id)}}>
        <x-svg svg="file" fill="#000000" viewBox="24 24" class="file-svg mq-max-m"/>
        <p class="filename">{{$file->filename}}</p>
      </a>
    </div>
    <div class="small-row margin-20 right">
      <p class="light medium">{{$file->created_at->translatedFormat("d.m.Y")}}</p>
    </div>

    @can("administration")

    <div class="small-row"><a class="invisible-link margin-20 right"
        href={{route("admin.file.edit", $file->id)}}>
        <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit"/>
      </a>

      <div class="checkbox-container margin-20 right">
        <input class="checkbox" type="checkbox" name="files[{{$loop->index}}][selected]" onclick="ShowAndHideButtons()"
          id={{$file->id}} value="1">
        <span class="checkmark"></span>
      </div>
    </div>

    @endcan

  </div>
  <div class="description-box medium" id="description{{$file->id}}">
    @if($file->description)
    <p>{{$file->description}}</p>
    @else
    <p class="small light">Für diese Datei gibts keine Beschreibung</p>
    @endif
  </div>
</div>
@endforeach
@can("administration")
{{Form::close()}}
@endcan
@else
<p>Dieser Ordner enthält keine Dateien</p>
@endif
@endsection