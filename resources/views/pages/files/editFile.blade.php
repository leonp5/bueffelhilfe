@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>Datei "{{$file->filename}}" bearbeiten</h2>
{!! Form::open(["route" => ["admin.file.update", $file->id], "class" => "form-container", "method" => "PUT", 'enctype'
=>
'multipart/form-data']) !!}

@include('pages.files.inc.fileForm')


<div class="row-space-between margin-30 top"><a class="abort-btn" href="{{url()->previous()}}">Abbrechen</a>
  <button id="FileUploadSubmit" class="action-btn" type="submit" disabled>Änderungen übernehmen</button>
</div>

{!! Form::close() !!}


@endsection