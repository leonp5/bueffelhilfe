@extends('layouts.app')

@section('content')

<h2>Ordner</h2>
@can('administration')
<a class="floating-add-file" href="{{route("admin.file.create")}}">
  <x-svg svg="file" fill="#000000" viewBox="24 24" class="file-svg"/>
</a>
<a class="floating-add-folder" href="{{route("admin.folder.create")}}">
  <x-svg svg="folder" fill="#000000" width=26 height=26 viewBox="26 26"/>
</a>
@endcan

@if ($personalFolder == true)

<div class="folder-wrapper">
  <div class="folder">
    <a class="small-row folder-icon" href={{route("personalFiles.show")}}>
      <x-svg svg="folder" fill="#000000" width=26 height=26 viewBox="26 26"/>
      <p>Deine Dateien</p>
    </a>
  </div>
  <p class="small">In diesem Ordner sind Dateien, die nur du sehen kannst.</p>
</div>

@endif

<h3 class="margin-50">Allgemeine Dateien</h3>

<div class="row-wrap" id="toggleDeleteIsNecessary">
  @forelse ($folders as $folder)
  <div class="folder-wrapper margin-20">
    <div class="folder">
      <div class="row-space-between">
        <a class="align-left folder-icon" href={{route("files.show", $folder->id)}}>
          <x-svg svg="folder" fill="#000000" width=26 height=26 viewBox="26 26"/>
          <p>{{$folder->name}} ({{$folder->count}})</p>
        </a>
        @can("administration")
        <div class="small-row">
          <a class="invisible-link" href={{route("admin.addFileToFolder", $folder->id)}}>
          <x-svg svg="plus" viewBox="469.33333 469.33333" class="plus margin-10 right"/>
          </a>
          <a class="invisible-link" href={{route("admin.folder.edit", $folder->id)}}>
            <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit"/>
          </a>
          <button class="table-button" onclick="ToggleDeleteModal({{$folder}})">
            <x-svg svg="trash" width=36 height=36 viewBox="24 24" class="delete"/>
          </button>
        </div>

        @endcan

      </div>
      @if($folder->visible == 0)
      <p class="small light">Nicht für Schüler sichtbar</p>
      @endif
    </div>
    @if($folder->description)
    <div class="small-row align-left clickable" onclick="ToggleDescriptionBox({{$folder->id}})">

      <span class="arrow-right" id="arrow{{$folder->id}}">
        <span></span>
        <span></span>
      </span>
      <p class="small">Beschreibung anzeigen</p>
    </div>
    <div class="description-box medium" id="description{{$folder->id}}">
      <p>{{$folder->description}}</p>
    </div>
    @else
    <p class="small light">Für diesen Ordner gibts keine Beschreibung</p>
    @endif
  </div>
  @empty
  <p>Es gibt keine Ordner mit Dateien.</p>
  @endforelse
</div>


@endsection