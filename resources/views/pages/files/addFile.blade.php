@extends('layouts.app')

@section('content')

@include("inc.backButton")

<h2>Datei hochladen</h2>
{!! Form::open(["route" => "admin.file.store", "class" => "form-container", "method" => "POST", 'enctype' =>
'multipart/form-data']) !!}

@include('pages.files.inc.fileForm')

<div class="row-space-between margin-30 top"><a class="abort-btn" href="{{route("files.index")}}">Abbrechen</a>
  <button id="FileUploadSubmit" class="action-btn" type="submit" disabled>Datei hochladen</button>
</div>

{!! Form::close() !!}


@endsection