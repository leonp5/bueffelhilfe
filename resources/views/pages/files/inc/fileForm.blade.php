@if($file ?? "")

<p id="saved-file">Gespeicherte Datei:</p>
<p class="medium margin-30 bottom" id="old-file-text">{{$file->filename}}</p>

@endif

<div class="dragNdrop column" id="dragNdrop">
  <label class="custom-file-upload margin-30 bottom" for="file-input">
    <input type="file" name="filename" id="file-input" onchange="HandleFileUpload()" />
    Wähle eine Datei</label>
  <span id="dragNdrop-text">oder leg sie hier ab</span>
</div>

<span class="file-upload-label" id="file-selected"></span>

{{Form::label("description", "Beschreibung", ["class" => "input-label"])}}

{{Form::textarea("description", $file->description ?? "", ["class" => "textarea"])}}

<div class="column" >

  @if($folder ?? "")

  <p>Datei in den Ordner "{{$folder->name}}" hochladen</p>
  <input type="hidden" name="folderID" value={{$folder->id}}>


  @else

  <p class="medium">Dateien können entweder einem Nutzer oder einem allgemeinen Ordner zugeordnet
    werden.</p>

  <div class="custom-select">
    {{Form::select('userID', $users->pluck("name", "id"), $file->userID ?? null,['placeholder' => 'Benutzer auswählen', "id" => "username", "onchange" => "HandleFileUpload()"])}}
  </div>
  <p>oder</p>
  <div class="custom-select">
    {{Form::select('folderID', $folders->pluck("name", "id"), $file->folderID ?? null,['placeholder' => 'Ordner auswählen', "id" => "folder", "onchange" => "HandleFileUpload()"])}}
  </div>
</div>
@endif
