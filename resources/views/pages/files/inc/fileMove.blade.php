<div class="move-modal" id="move-modal">
  Zielordner auswählen

  <div class="custom-select margin-30 top">
    {{Form::select('folderID', $folders->pluck("name", "id"), null,['placeholder' => 'Ordner auswählen', "id" => "folder", "onchange" => "ValidateSelectFolder()"])}}
  </div>

  <div class="row-space-between margin-30 top"><a class="abort-btn" onclick="ToggleMoveModal()">Abbrechen</a>
    <button id="move-files-btn" class="action-btn" type="submit" disabled>Dateien verschieben</button>
  </div>
</div>