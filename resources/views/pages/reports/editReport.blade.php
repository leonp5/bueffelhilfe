@extends('layouts.app')

@section('content')

<h2>Feedback bearbeiten</h2>

{!! Form::open(["route" => ["admin.reports.update", $report], "method" => "PUT", "class" => "form-container", "id" =>
"report-content"]) !!}

<h3> Feedback von "{{$report->name}}" bearbeiten</h3>

{{Form::label("name", "Name", ["class" => "input-label"])}}
{{Form::text("name", $report->name, ["class" => "report-input", "placeholder" => "Name"])}}

{{Form::label("body", "Feedback", ["class" => "input-label"])}}
{{Form::textarea("body", $report->body, ["class" => "textarea"])}}

<p class="small">Mindestens 100 Zeichen.</p>

<div class="row-grid">
  <label class="input-label">Bewertung</label>

  <div class="clickable-star-rating" id="rating" onclick="ValidateInput()">
    <input type="radio" id="star5" name="rating" value="5" {{( $report->rating == 5 ? "checked" : "")}} /><label
      for="star5"></label>
    <input type="radio" id="star4" name="rating" value="4" {{( $report->rating == 4 ? "checked" : "")}} /><label
      for="star4"></label>
    <input type="radio" id="star3" name="rating" value="3" {{( $report->rating == 3 ? "checked" : "")}} /><label
      for="star3"></label>
    <input type="radio" id="star2" name="rating" value="2" {{( $report->rating == 2 ? "checked" : "")}} /><label
      for="star2"></label>
    <input type="radio" id="star1" name="rating" value="1" {{( $report->rating == 1 ? "checked" : "")}} /><label
      for="star1"></label>
  </div>
</div>

<div class="row-space-between margin-30 top">
  <label for="visible">Sichtbar</label>
  <div class="checkbox-container">
    <input class="checkbox" type="checkbox" name="visible" value="1" {{( $report->visible == 1 ? "checked" : "")}}>
    <span class="checkmark"></span>
  </div>
  <label class="checkbox-label" for="show_in_slider">Im Slider</label>
  <div class="checkbox-container">
    <input class="checkbox" type="checkbox" name="show_in_slider" value="1"
      {{($report->show_in_slider == 1 ? "checked" : "")}}>
    <span class="checkmark"></span>
  </div>
</div>
<div class="row-space-between margin-30 top">
  <a class="abort-btn" href="{{route("admin.reports.index")}}">Abbrechen</a>
  <button class="action-btn" id="send-report">Speichern</button>
</div>
{!! Form::close() !!}

@endsection