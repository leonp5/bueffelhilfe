<div class="report-modal">
  <input type="checkbox" class="img-close" onClick="ToggleNewReport()" />
  <a class="img-close-cross"></a>
  <div class="report-modal-content" id="report-content">
    <h2>
      @guest Feedback geben @endguest
      @can("administration")
      Neuer Eintrag
      @endcan
    </h2>

    <label class="input-label">Name</label>
    <input autofocus class="report-input" oninput="ValidateInput()" id="validation-necessary">
    <label class="input-label">Feedback</label>
    <textarea oninput="ValidateInput()" class="textarea"></textarea>
    <p class="small">Mindestens 50 Zeichen.</p>
    <div class="row-grid">
      <label class="input-label">Bewertung</label>

      @include('pages.reports.inc.starRating')
    </div>
    @can("administration")
    <div class="row-space-between margin-30 top">
      <label for="visible">Sichtbar</label>
      <div class="checkbox-container">
        <input class="checkbox" type="checkbox" name="visible">
        <span class="checkmark"></span>
      </div>
      <label class="checkbox-label" for="show_in_slider">Im Slider</label>
      <div class="checkbox-container">
        <input class="checkbox" type="checkbox" name="show_in_slider">
        <span class="checkmark"></span>
      </div>
    </div>
    @endcan
    <div class="row-space-between margin-30 top">
      <button class="abort-btn" onClick="ToggleNewReport()">Abbrechen</button>
      <button class="action-btn" id="send-report" onclick="PostData()" disabled>Speichern</button>
    </div>
  </div>
</div>