@extends('layouts.app')

@section('content')

<h2>Feedback</h2>

@can("administration")

@if(count($newReports) > 0)

<h2>Neue Feedback Einträge</h2>

{!! Form::open(["route" => ["admin.reports.multiupdate"], "method" => "PUT", "class" => "report-table"]) !!}
<div class="th-row">
  <div class="th-author">Name</div>
  <div class="th-flex">Feedback</div>
  <div class="th-wrapper">
    <div class="th-visible">
      <p class="th-text">Sichtbar</p>
    </div>
    <div class="th-slider">
      <p class="th-text">Im Slider</p>
    </div>
    <div class="th-small">Bearbeiten</div>
  </div>
</div>


@foreach ($newReports as $newReport)
<div class="report-tr">
  <input type="hidden" name="reports[{{$loop->index}}][id]" value="{{$newReport->id}}">
  <div class="td-name">
    <p class="td-text">{{$newReport->name}}</p>
    <div class="show-star-rating-sm" style="--rating: {{$newReport->rating}}">
    </div>
    <div class="show-star-rating-l" style="--rating: {{$newReport->rating}}">
    </div>
  </div>
  <div class="td-flex">{{$newReport->body}}</div>
  <div class="tr-wrapper">
    <div class="form-wrapper">
      <div class="checkbox-wrapper">
        <div class="checkbox-visible">
          <div class="checkbox-container">
            <input class="checkbox" type="checkbox" name="reports[{{$loop->index}}][visible]" value="1" checked>
            <span class="checkmark"></span>
          </div>
          <label class="table-label" for="visible">Sichtbar</label>
        </div>
        <div class="checkbox-slider">
          <div class="checkbox-container">
            <input class="checkbox" type="checkbox" name="reports[{{$loop->index}}][show_in_slider]" value="1"
              {{($newReport->show_in_slider == 1 ? "checked" : "")}}>
            <span class="checkmark"></span>
          </div>
          <label class="table-label" for="show_in_slider">Im Slider</label>
        </div>

      </div>
    </div>

    <a class="table-link table-edit" href={{route("admin.reports.edit", $newReport->id)}}>
      <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="table-svg" />
    </a>

  </div>
</div>
@endforeach
<button class="floating-save">
  <x-svg svg="saveAll" id="path90" height="36" width="36" viewBox="32 32" class="saveAll" />
</button>
{!! Form::close() !!}

@endif

@endcan
<div class="row">
  <button class="action-btn mq-max-m" onClick="ToggleNewReport()">
    @cannot("administration")
    Feedback geben
    @endcannot
    @can("administration")
    Neuer Eintrag
    @endcan
  </button>
  <div class="small-row">
    <div class="average-star-rating" style="--rating: {{$reportsStats["average"]}};">
    </div>
    <p class="small">Ø {{$reportsStats["average"]}}/5</p>
  </div>
  <p>Insgesamt {{$reportsStats["countedReports"]}} Bewertungen.</p>
  @can("administration")
  <p class="small">
    (Davon {{$reportsStats["visibleReports"]}} sichtbar Ø {{$reportsStats["visibleReportsAverage"]}}/5)
  </p>
  @endcan

</div>

<div class="floating-add mq-min-m" onClick="ToggleNewReport()"></div>

@include('pages.reports.newReport')

<div class="report-table">

  <div class="th-row">
    <div class="th-author">Name</div>
    <div class="th-flex">Feedback</div>

    @cannot("administration")

    <div class="th-rating">Bewertung</div>

    @endcannot

    @can("administration")
    <div class="th-wrapper">
      <div class="th-visible">
        <p class="th-text">Sichtbar</p>
      </div>
      <div class="th-slider">
        <p class="th-text">Im Slider</p>
      </div>
      <div class="th-small">Aktionen</div>
    </div>
    @endcan
  </div>

  @foreach ($reports as $report)
  <div class="report-tr">

    <div class="td-name">
      <p class="td-text">{{$report->name}}</p>

      @can("administration")
      <div class="show-star-rating-sm" style="--rating: {{$report->rating}}">
      </div>
      <div class="show-star-rating-l" style="--rating: {{$report->rating}}">
      </div>
      @endcan
    </div>
    <div class="td-flex">{{$report->body}}</div>

    @cannot("administration")
    <div class="tr-wrapper-guest">
      <div class="show-star-rating-guest" style="--rating: {{$report->rating}};">

      </div>
    </div>
    @endcannot

    @can("administration")
    <div class="tr-wrapper">

      {!! Form::open(["route" => ["admin.reports.checkboxupdate", $report], "method" => "PUT", "class" =>
      "form-wrapper"])
      !!}
      <input type="hidden" name="id" value="{{$report->id}}">
      <div class="checkbox-wrapper">
        <div class="checkbox-visible">
          <div class="checkbox-container">
            <input class="checkbox" type="checkbox" name="visible[]" value="1"
              {{( $report->visible == 1 ? "checked" : "")}}>
            <span class="checkmark"></span>
          </div>
          <label class="table-label" for="visible">Sichtbar</label>
        </div>
        <div class="checkbox-slider">
          <div class="checkbox-container">
            <input class="checkbox" type="checkbox" name="show_in_slider[]" value="1"
              {{($report->show_in_slider == 1 ? "checked" : "")}}>
            <span class="checkmark"></span>
          </div>
          <label class="table-label" for="show_in_slider">Im Slider</label>
        </div>
      </div>
      <button class="table-button">
        <x-svg svg="save" width=36 height=36 viewBox="512 512" class="save"/>
      </button>

      {!! Form::close() !!}

      <a class="table-link table-edit" href={{route("admin.reports.edit", $report->id)}}>
        <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit" />
      </a>
      {!! Form::open(["action" => ["ReportsController@destroy",$report], "method" => "DELETE", "class" =>
      "table-delete"]) !!}
      <button class="table-button">
        <x-svg svg="trash" fill="none" width=36 height=36 viewBox="24 24" class="delete" />

      </button>
      {!!Form::close()!!}

    </div>
    @endcan

  </div>
  @endforeach
</div>

@endsection