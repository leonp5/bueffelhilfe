@can('administration')
<div class="align-right"><a class="table-button" href="{{route("reports")}}">
    <x-svg svg="pencil" fill="none" width=36 height=36 viewBox="24 24" class="edit-active" />
  </a>
</div>
@endcan

<div class="slider-wrapper">
  @if($slides ?? "")
  @foreach ($slides as $slide)
  <div class="slide">
    <div class="show-star-rating-m" style="--rating: {{$slide->rating}}"></div>
    <p class="slider-text">"{{$slide->body}}"</p>
    <p class="slider-name">{{$slide->name}}</p>
  </div>

  @endforeach
  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>

<div class="dots-wrapper">
  @foreach ($slides as $slide)
  <span class="dot" onclick="currentSlide({{$slide->number}})"></span>
  @endforeach
  @endif
</div>