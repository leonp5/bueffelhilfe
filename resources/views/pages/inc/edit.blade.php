@extends('layouts.app')

@section('content')

@include("inc.backButton")

{!! Form::open(["route" => ["admin.content.update", $content], "method" => "PUT", "class" => "form-container-large"])
!!}

@if ($content->id == 4)

<h3>"{{$content->title}}" bearbeiten</h3>

{{Form::label("title", "Überschrift", ["class" => "input-label"])}}
{{Form::text("title", $content->title, ["class" => "report-input-large", "placeholder" => "Überschrift"])}}

{{Form::label("body", "Nummer", ["class" => "input-label"])}}
{{Form::text("body", $content->body, ["class" => "report-input-large"])}}

{{Form::label("email", "Email", ["class" => "input-label"])}}
{{Form::text("email", $content->email, ["class" => "report-input-large"])}}

@elseif($content->id == 5)

<h3>"Dreizeiler" bearbeiten</h3>

{{Form::label("title", "Erste Zeile", ["class" => "input-label"])}}
{{Form::text("title", $content->title, ["class" => "report-input-large", "placeholder" => "Überschrift"])}}

{{Form::label("body", "Zweite Zeile", ["class" => "input-label"])}}
{{Form::text("body", $content->body, ["class" => "report-input-large"])}}

{{Form::label("email", "Dritte Zeile", ["class" => "input-label"])}}
{{Form::text("email", $content->email, ["class" => "report-input-large"])}}

@else

<h3>"{{$content->title}}" bearbeiten</h3>

{{Form::label("title", "Überschrift", ["class" => "input-label"])}}
{{Form::text("title", $content->title, ["class" => "report-input-large", "placeholder" => "Überschrift"])}}

{{Form::label("body", "Text", ["class" => "input-label"])}}
{{Form::textarea("body", $content->body, ["class" => "textarea"])}}

@endif

<div class="row-space-between margin-30 top">
  <a class="abort-btn" href="{{route("main")}}">Abbrechen</a>
  <button class="action-btn">Speichern</button>
</div>

{!! Form::close() !!}

@endsection