<div class="navbar" id="navbar">
  <a class="{{Request::path() === "/" ? "navbar-link-active" : "navbar-link" }}" onClick="ScrollTo(`start`)"
    name="start">START</a>

  @can("administration")
  <a class="{{Request::path() === "admin/users" ? "navbar-link-active" : "navbar-link" }}"
    href="{{route("admin.users.index")}}">Benutzerverwaltung</a>
  @endcan


  @auth
  <a class="{{Request::path() === "folders" ? "navbar-link-active" : "navbar-link" }}"
    href="{{route("files.index")}}">Lernmaterial</a>
  <a class="{{Request::path() === "reports" ? "navbar-link-active" : "navbar-link" }}"
    href="{{route("reports")}}">FEEDBACK
    @can("administration")
    @if($numberOfNewReports ?? "")
    ({{$numberOfNewReports}})
    @endif
    @endcan
  </a>
  @endauth

  @guest
  <a class="navbar-link" onClick="ScrollTo(`lerninhalte`)" name="lerninhalte">{{$titles[1]->title}}</a>
  <a class="navbar-link" onClick="ScrollTo(`uebermich`)" name="uebermich">{{$titles[2]->title}}</a>
  <a class="navbar-link" onClick="ScrollTo(`kontakt`)" name="kontakt">{{$titles[3]->title}}</a>
  <a class="{{Request::path() === "reports" ? "navbar-link-active" : "navbar-link" }}"
    href="{{route("reports")}}">FEEDBACK</a>
  <a class="desktop-login-link" onClick="ToggleDesktopLogin()">LOGIN</a>

  @else
  <a class="navbar-link" href="{{ route('logout') }}" onclick="event.preventDefault();
 document.getElementById('logout-form').submit();">
    {{ __('LOGOUT') }}</a>
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>

  @endguest

</div>