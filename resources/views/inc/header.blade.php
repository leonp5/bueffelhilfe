<div class="header mq-max-m">
  <div class="max-width">
    <a class="invisible-link" href="/">
      <h1 class="logo">Büffelhilfe</h1>
    </a>
    {{-- <div class="navbar"> --}}

    @include('inc.navigationLinks')

    {{-- </div> --}}
  </div>
</div>