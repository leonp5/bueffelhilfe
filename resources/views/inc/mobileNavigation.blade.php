<button onClick="ToggleMenu()" class="mobile-menu-btn mq-min-m">
  <div class="btn-line"></div>
  <div class="btn-line"></div>
  <div class="btn-line"></div>

</button>

<div class="mobile-menu" id="mobile-menu">
  <input type="checkbox" class="close-button" onClick="ToggleMenu()" />
  <a class="close-cross"></a>
  @include('inc.navigationLinks')
</div>