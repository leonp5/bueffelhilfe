{!! Form::open(["action" => "Auth\LoginController@login", "method" => "POST", "class"=>"desktop-login",
"id" => "login-container"]) !!}

{{Form::text("name", old("name"),  ["class" => "basic-input", "placeholder" => "Name", "id" => "name"])}}

{{Form::password("password", ["class" => "basic-input", "placeholder" => "Passwort", "id" => "password"])}}

<div class="row-grid">
  <div class="checkbox-container">
    <input class="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
    <label class="checkmark"></label>
  </div>
  {{Form::label("remember", "Merken", ["class" => "form-label"])}}
</div>
{{Form::submit("Anmelden", ["class" => "basic-btn"])}}

{!! Form::close() !!}