@guest
<button class="floating-btn mq-min-m" id="mobile-login-btn" onClick="ToggleMobileLogin()">Login</button>
@endguest


<div class="modal-background" id="modal-background">
  <div class=login-modal id="login-modal">
    <input type="checkbox" class="close-button" onClick="ToggleMobileLogin()" />
    <a class="close-cross"></a>
    {!! Form::open(["action" => "Auth\LoginController@login", "method" => "POST", "class" => "modal-content"]) !!}
    <div class="column" style="grid-row: 1 / 2">
      {{Form::text("name", old("name"),  ["class" => "basic-input", "placeholder" => "Name", "id" => "name"])}}

      {{Form::password("password", ["class" => "basic-input", "placeholder" => "Passwort", "id" => "password"])}}
    </div>
    <div class="row-grid" style="grid-row: 2 / 3">
      <div class="checkbox-container">
        <input class="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        <label class="checkmark"></label>
      </div>
      {{Form::label("remember", "Merken", ["class" => "form-label"])}}
    </div>
    {{Form::submit("Anmelden", ["class" => "login-modal-btn"])}}

    {!! Form::close() !!}
  </div>
</div>