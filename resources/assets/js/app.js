import ToggleImage from "./components/ToggleImage";
import ToggleMenu from "./components/ToggleMenu";
import { ScrollTo, GetPathIDfromSessionStorage } from "./components/ScrollTo";
import {
    ToggleDesktopLogin,
    ToggleMobileLogin
} from "./components/ToggleLogin";
import FadeLoginButton from "./components/FadeLoginButton";
import PostData from "./components/reports/PostData";
import ToggleNewReport from "./components/reports/ToggleNewReport";
import ValidateInput from "./components/reports/ValidateInput";
import StarRating from "./components/reports/StarRating";
import {
    plusSlides,
    currentSlide,
    showSlides,
    AutomaticSlideAndSwipe
} from "./components/Slider";

import RemoveAlert from "./components/RemoveAlert";
import ToggleDescriptionBox from "./components/ToggleDescriptionBox";
import ToggleDeleteModal from "./components/ToggleDeleteModal";
import { CookieModal, ConfirmCookie } from "./components/CookieModal";
import ToggleMoveModal from "./components/files/ToggleMoveModal";
import HandleFileUpload from "./components/files/HandleFileUpload";
import DragNDrop from "./components/files/DragNDrop";
import ShowAndHideButtons from "./components/files/ShowAndHideButtons";
import ValidateSelectFolder from "./components/files/ValidateSelectFolder";
import ValidateFolderName from "./components/files/ValidateFolderName";
import ToggleSelectAll from "./components/files/ToggleSelectAll";

// onload

window.onload = FadeLoginButton();
document.onload = CookieModal();

if (document.location.pathname === "/") {
    // slider
    window.onload = AutomaticSlideAndSwipe();

    // for page navigation
    window.onload = GetPathIDfromSessionStorage();
}

// get star rating value
if (document.body.contains(document.getElementById("rating"))) {
    window.onload = StarRating();
}

document.ToggleMenu = ToggleMenu;
document.ScrollTo = ScrollTo;
window.ToggleMobileLogin = ToggleMobileLogin;
window.ToggleDesktopLogin = ToggleDesktopLogin;
window.PostData = PostData;
window.ToggleNewReport = ToggleNewReport;
document.RemoveAlert = RemoveAlert;

// landing page

if (document.location.pathname === "/") {
    window.ToggleImage = ToggleImage;
    document.showSlides = showSlides;
    document.plusSlides = plusSlides;
    document.currentSlide = currentSlide;
}

// if (
//     document.location.pathname === "/admin/folder/create" ||
//     "/admin/file/create"
// ) {
//     window.onload = CookieModal();
// }

// charValidation

if (document.body.contains(document.getElementById("validation-necessary"))) {
    window.ValidateInput = ValidateInput;
}

// validate file upload

if (document.body.contains(document.getElementById("file-input"))) {
    window.onload = () => {
        HandleFileUpload();
        DragNDrop();
    };
    window.HandleFileUpload = HandleFileUpload;
}

// toggle folder description

if (document.location.pathname === "/folders") {
    window.ToggleDescriptionBox = ToggleDescriptionBox;
}

// toggle file description

if (window.location.href.indexOf("files") > -1) {
    window.ToggleDescriptionBox = ToggleDescriptionBox;
}

// toggle user delete modal

if (
    document.body.contains(document.getElementById("toggleDeleteIsNecessary"))
) {
    window.ToggleDeleteModal = ToggleDeleteModal;
}

// validate folder create/edit form name

if (
    document.body.contains(document.getElementById("folderNameCheckNecessary"))
) {
    window.onload = ValidateFolderName;
    window.ValidateFolderName = ValidateFolderName;
}

// show and hide delete/move file buttons

if (
    document.body.contains(
        document.getElementById("ShowAndHideButtonsNecessary")
    )
) {
    window.ShowAndHideButtons = ShowAndHideButtons;
}

if (document.body.contains(document.getElementById("cookie-modal"))) {
    window.ConfirmCookie = ConfirmCookie;
}

if (document.body.contains(document.getElementById("floating-move"))) {
    window.ToggleMoveModal = ToggleMoveModal;
    window.ValidateSelectFolder = ValidateSelectFolder;
}

if (
    document.body.contains(
        document.getElementById("toggleSelectAllIsNecessary")
    )
) {
    window.ToggleSelectAll = ToggleSelectAll;
}
