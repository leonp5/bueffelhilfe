export default function RemoveAlert() {
    const alert = document.querySelector("div[class=alert-modal]");
    const warning = document.querySelector("div[class=warning-modal]");
    if (alert) {
        alert.style.display = "none";
    }
    if (warning) {
        warning.style.display = "none";
    }
}
