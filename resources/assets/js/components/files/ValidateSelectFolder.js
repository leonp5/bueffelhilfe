export default function ValidateSelectFolder() {
    const button = document.getElementById("move-files-btn");
    const folderInput = document.getElementById("folder");
    const selectedFolder = folderInput.options[folderInput.selectedIndex].value;

    if (selectedFolder === "") {
        button.disabled = true;
    } else {
        button.disabled = false;
    }
}
