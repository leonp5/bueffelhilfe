export default function HandleFileUpload() {
    // Show filename of (new) uploaded file & check if savedFile exists

    const fileInputName = document.getElementById("file-input").value;
    const savedFileExists = document.getElementById("saved-file");
    const submitButton = document.getElementById("FileUploadSubmit");

    function HandleFileInput() {
        if (fileInputName !== "") {
            const fileSelected = document.getElementById("file-selected");
            const fileName = fileInputName.split("\\").pop();
            fileSelected.innerHTML = fileName;
            fileSelected.style.color = "#049b04";
            if (savedFileExists && fileInputName !== "") {
                const oldFileText = document.getElementById("old-file-text");
                oldFileText.style.color = "red";
            }
            return true;
        } else {
            return false;
        }
    }
    const fileInput = HandleFileInput();

    // check, which upload form is rendered

    const userNameInput = document.getElementById("username");
    if (userNameInput) {
        const selectedUserName =
            userNameInput.options[userNameInput.selectedIndex].value;
        const folderInput = document.getElementById("folder");
        const selectedFolder =
            folderInput.options[folderInput.selectedIndex].value;

        // manage enabling & disabling submit button

        if (selectedFolder) {
            userNameInput.disabled = true;
        }

        if (selectedFolder === "") {
            userNameInput.disabled = false;
        }

        if (selectedUserName) {
            folderInput.disabled = true;
        }

        if (selectedUserName === "") {
            folderInput.disabled = false;
        }

        if (
            (selectedUserName || selectedFolder) &&
            (fileInput === true || savedFileExists)
        ) {
            submitButton.disabled = false;
        } else {
            submitButton.disabled = true;
        }
    } else {
        if (fileInput === true) {
            submitButton.disabled = false;
        } else {
            submitButton.disabled = true;
        }
    }
}
