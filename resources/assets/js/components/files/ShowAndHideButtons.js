export default function ShowAndHideButtons() {
    // get the inputs which contains "files" in the name attribute AND have type = checkbox

    const checkboxes = document.querySelectorAll(
        "input[name^=files][type=checkbox]"
    );
    const deleteButton = document.getElementById("floating-delete");
    const moveButton = document.getElementById("floating-move");
    const container = document.getElementById("toggleSelectAllIsNecessary");
    const toggleAll = container.getElementsByTagName("input")[0];
    const text = container.getElementsByTagName("p")[0];

    // You need the Array.prototype.slice.call part to convert the NodeList returned by document.querySelectorAll into an array that you can call some on.

    const checkedOne = Array.prototype.slice
        .call(checkboxes)
        .some(x => x.checked);

    if (checkedOne) {
        deleteButton.style.display = "flex";
        if (moveButton) {
            moveButton.style.display = "flex";
        }
    } else {
        deleteButton.style.display = "none";
        if (moveButton) {
            moveButton.style.display = "none";
        }
    }

    const unCheckedOne = Array.prototype.slice
        .call(checkboxes)
        .some(x => !x.checked);

    if (unCheckedOne) {
        text.innerHTML = "Alle auswählen";
        toggleAll.checked = false;
    }

    if (!unCheckedOne) {
        toggleAll.checked = true;
        text.innerHTML = "Alle abwählen";
    }

    let i = 0;

    for (i = 0; i < checkboxes.length; ++i) {
        const id = checkboxes[i].id;
        const folder = document.querySelector(`div[id="${id}"]`);
        if (checkboxes[i].checked) {
            folder.style.background = "#00000080";
        } else {
            folder.style.background = "initial";
        }
    }
}
