import HandleFileUpload from "./HandleFileUpload";

export default function DragNDrop() {
    // check if the browser supports drag and drop

    const canDragNDrop = (function() {
        const div = document.createElement("div");
        return (
            ("draggable" in div || ("ondragstart" in div && "ondrop" in div)) &&
            "FormData" in window &&
            "FileReader" in window
        );
    })();

    if (canDragNDrop) {
        const box = document.getElementById("dragNdrop");
        box.classList.add("dragNdrop-avalaible");

        function preventDefault(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        function dragOver(e) {
            preventDefault(e);
            box.classList.add("dragover");
        }

        function dragLeave(e) {
            preventDefault(e);
            box.classList.remove("dragover");
        }

        function handleDrop(e) {
            dragLeave(e);
            const fileInput = document.getElementById("file-input");
            const dataTransfer = new DataTransfer();
            dataTransfer.items.add(e.dataTransfer.files[0]);
            fileInput.files = dataTransfer.files;
            HandleFileUpload();
        }

        box.addEventListener("drag", preventDefault);
        box.addEventListener("dragstart", preventDefault);
        box.addEventListener("dragover", dragOver);
        box.addEventListener("dragenter", dragOver);
        box.addEventListener("dragend", dragLeave);
        box.addEventListener("dragleave", dragLeave);
        box.addEventListener("drop", handleDrop);
    }
}
