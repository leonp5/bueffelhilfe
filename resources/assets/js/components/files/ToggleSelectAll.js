import ShowAndHideButtons from "./ShowAndHideButtons";

export default function ToggleSelectAll() {
    const checkboxes = document.querySelectorAll(
        "input[name^=files][type=checkbox]"
    );

    const container = document.getElementById("toggleSelectAllIsNecessary");

    const toggleAll = container.getElementsByTagName("input")[0].checked;

    const text = container.getElementsByTagName("p")[0];

    let i = 0;
    for (i = 0; i < checkboxes.length; ++i) {
        if (toggleAll === true) {
            checkboxes[i].checked = true;
            text.innerHTML = "Alle abwählen";
        }
        if (toggleAll === false) {
            checkboxes[i].checked = false;
            text.innerHTML = "Alle auswählen";
        }
    }
    ShowAndHideButtons();
}
