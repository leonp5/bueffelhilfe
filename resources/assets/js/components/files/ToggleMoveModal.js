export default function ToggleMoveModal() {
    const form = document.getElementById("editAndDeleteForm");
    const input = form.querySelector("input[type=hidden][name=_method]");
    const modalBG = document.getElementById("modal-background");
    modalBG.classList.toggle("modal-background-open");
    const modal = document.getElementById("move-modal");
    const folderInput = document.getElementById("folder");
    folderInput.value = "";
    if (modal.style.display === "flex") {
        modal.style.display = "none";
        input.value = "DELETE";
    } else {
        modal.style.display = "flex";
        input.value = "PUT";
    }
}
