export default function ValidateFolderName() {
    const folderName = document.getElementById("folderNameCheckNecessary");
    const submitButton = document.getElementsByClassName("action-btn")[0];

    if (folderName.value.length < 3) {
        submitButton.disabled = true;
    } else {
        submitButton.disabled = false;
    }
}
