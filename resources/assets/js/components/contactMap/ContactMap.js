function checkUserAgent() {
    if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            navigator.userAgent
        )
    ) {
        return false;
    } else {
        return true;
    }
}

const pc = checkUserAgent();

const position = [50.95258, 6.91488];

const map = L.map("map", {
    scrollWheelZoom: false,
    doubleClickZoom: false,
    dragging: pc
}).setView(position, 12);

L.tileLayer("https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png", {
    attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

const circle = L.circle(position, {
    color: "#e54400",
    fillColor: "#e54400",
    fillOpacity: 0.2,
    radius: 2600
}).addTo(map);

// const marker = L.marker(position).addTo(map);
// marker.bindPopup("<b> Meine Adresse:</b><br> Senefelderstraße 16, 50825 Köln");
