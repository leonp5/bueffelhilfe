export default function ToggleMenu() {
    const menu = document.getElementById("mobile-menu");
    if (menu.style.display === "flex") {
        document.removeEventListener("click", ClickOutside, true);
        menu.classList.remove("mobile-menu-open");
        setTimeout(() => {
            menu.style.display = "none";
        }, 600);
    } else {
        menu.style.display = "flex";
        document.addEventListener("click", ClickOutside, true);
        setTimeout(() => {
            menu.classList.add("mobile-menu-open");
        }, 10);
    }
}

function ClickOutside(event) {
    const menu = document.getElementById("mobile-menu");
    if (event.target !== menu) {
        ToggleMenu();
    }
}
