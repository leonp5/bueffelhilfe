export async function ScrollTo(id) {
    const currentPath = document.location.pathname;
    if (currentPath !== "/") {
        sessionStorage.setItem("id", JSON.stringify(id));
        document.location.pathname = "/";
    } else {
        scroll(id);
        document
            .getElementById("mobile-menu")
            .classList.remove("mobile-menu-open");
    }
}

export function GetPathIDfromSessionStorage() {
    try {
        const item = sessionStorage.getItem("id");
        if (item) {
            const id = JSON.parse(item);
            scroll(id);
            sessionStorage.removeItem("id");
        }
    } catch (error) {
        console.error(error);
    }
}

function scroll(id) {
    history.pushState(null, null, "#" + id);
    ChangeLinkStyle(id);
    const currentSection = document.getElementById(id);
    currentSection.scrollIntoView({ behavior: "smooth" });
}

function ChangeLinkStyle(id) {
    const navbar = document.getElementById("navbar");
    const oldLink = navbar.querySelector("a[class=navbar-link-active]");

    oldLink.classList.remove("navbar-link-active");
    oldLink.classList.add("navbar-link");
    const currentLink = navbar.querySelector(`a[name=${id}`);

    currentLink.classList.remove("navbar-link");
    currentLink.classList.add("navbar-link-active");
}

// function IsElementInViewport(el) {
//     const rect = el.getBoundingClientRect();
//     return (
//         rect.bottom < 0 ||
//         rect.right < 0 ||
//         rect.left > window.innerWidth ||
//         rect.top > window.innerHeight
//     );
// }

// function OnVisibilityChange(id) {
//     let old_visible;
//     const el = document.getElementById(id);
//     const visible = IsElementInViewport(el);

//     if (visible != old_visible) {
//         old_visible = visible;
//         ChangeLinkStyle(id);
//     }
// }
