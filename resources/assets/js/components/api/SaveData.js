export default async function SaveData(api, data) {
    const token = document
        .querySelector('meta[name="csrf-token"]')
        .getAttribute("content");
    const url = window.location.origin + api;
    const response = await fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": token,
            Accept: "application/json"
        },
        body: JSON.stringify(data)
    });

    const result = await response.json();
    return result;
}
