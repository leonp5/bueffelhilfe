export default function ToggleImage() {
    const modalBG = document.getElementById("img-modal-background");
    const modalIMG = document.querySelector(".modal-img");
    const modalContent = document.querySelector(".img-modal-content");
    const img = "storage/images/david_menrath_bueffelhilfe.jpg";

    if (modalBG.style.display === "flex") {
        modalBG.style.opacity = "0";
        modalContent.style.transform = "scale(0)";
        setTimeout(() => {
            modalBG.style.display = "none";
            modalIMG.style.display = "none";
            modalIMG.setAttribute("src", "");
        }, 500);
    } else {
        modalBG.style.display = "flex";
        modalIMG.style.display = "initial";
        modalIMG.setAttribute("src", img);
        setTimeout(() => {
            modalBG.style.opacity = "1";
            modalContent.style.transform = "scale(1)";
        }, 10);
    }
}
