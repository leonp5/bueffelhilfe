export default function ToggleDeleteModal(data) {
    const modalBG = document.getElementById("modal-background");
    modalBG.classList.toggle("modal-background-open");

    if (document.getElementById("delete-modal")) {
        document.getElementById("delete-modal").remove();
    } else {
        const modal = document.createElement("div");
        modal.setAttribute("id", "delete-modal");
        modal.className = "delete-modal";

        const closeButton = document.createElement("input");
        modal.innerHTML =
            `"${data.name}"` + " mit allen Dateien unwideruflich löschen?";
        closeButton.className = "close-button";
        closeButton.onclick = ToggleDeleteModal;
        modal.appendChild(closeButton);

        const closeCross = document.createElement("a");
        closeCross.className = "close-cross";
        modal.appendChild(closeCross);

        const row = document.createElement("div");
        row.className = "row-space-between";

        const abortButton = document.createElement("button");
        abortButton.className = "abort-btn";
        abortButton.innerHTML = "Abbrechen";
        abortButton.onclick = ToggleDeleteModal;
        row.appendChild(abortButton);

        const deleteForm = document.createElement("form");
        deleteForm.setAttribute("method", "post");
        deleteForm.setAttribute("accept-charset", "UTF-8");
        // if the route at web.php gets changed, maybe this line must be modified to get the right url
        const url = window.location.href;
        deleteForm.setAttribute("action", url + "/" + data.id);

        const hiddenInput_1 = document.createElement("input");
        hiddenInput_1.setAttribute("name", "_method");
        hiddenInput_1.setAttribute("type", "hidden");
        hiddenInput_1.setAttribute("value", "DELETE");
        deleteForm.appendChild(hiddenInput_1);

        const hiddenInput_2 = document.createElement("input");
        hiddenInput_2.setAttribute("name", "_token");
        hiddenInput_2.setAttribute("type", "hidden");
        const token = document
            .querySelector('meta[name="csrf-token"]')
            .getAttribute("content");
        hiddenInput_2.setAttribute("value", token);
        deleteForm.appendChild(hiddenInput_2);

        row.appendChild(deleteForm);

        const submitButton = document.createElement("button");
        submitButton.className = "delete-btn";

        submitButton.innerHTML = "Löschen";
        deleteForm.appendChild(submitButton);

        modal.appendChild(row);
        modalBG.appendChild(modal);
    }
}
