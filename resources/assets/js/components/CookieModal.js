export function CookieModal() {
    document.getElementById("js-hint").remove();
    const cookieAccepted = localStorage.getItem("bueffelCookie");
    if (cookieAccepted === null) {
        const modalBG = document.createElement("div");
        modalBG.setAttribute("id", "cookie-modal");
        modalBG.className = "modal-background modal-background-open";

        const modal = document.createElement("div");
        modal.className = "cookie-modal";
        modalBG.appendChild(modal);

        const text = document.createElement("p");
        text.className = "center-text";
        text.innerHTML = `Diese Seite verwendet ausschließlich technische Cookies, die für den Betrieb der Website
        notwendig sind. <br> Es werden keine personenbezogenen Daten erhoben.`;
        modal.appendChild(text);

        const submitButton = document.createElement("button");
        submitButton.className = "basic-btn margin-20";
        submitButton.innerHTML = "In Ordnung!";
        submitButton.onclick = ConfirmCookie;
        modal.appendChild(submitButton);

        const mainContainer = document.getElementById("main-container");
        mainContainer.appendChild(modalBG);
    }
}

export function ConfirmCookie() {
    localStorage.setItem("bueffelCookie", 1);
    const modalBG = document.getElementById("cookie-modal");
    modalBG.classList.remove("modal-background-open");
    modalBG.remove();
}
