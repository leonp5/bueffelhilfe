export default function FadeLoginButton() {
    const width = window.innerWidth;
    if (width < 950) {
        document.addEventListener("touchstart", HandleTouchStart);
        document.addEventListener("touchmove", CheckScrollPosition);
    }
}

let touchstart = null;

function HandleTouchStart(e) {
    touchstart = e.touches[0].clientY;
}

function CheckScrollPosition(e) {
    if (e.touches.length === 1) {
        const touchend = e.changedTouches[0].clientY;

        const button = document.getElementById("mobile-login-btn");
        if (touchstart > touchend) {
            button.classList.add("floating-btn-show");
        } else {
            button.classList.remove("floating-btn-show");
        }
    }
}
