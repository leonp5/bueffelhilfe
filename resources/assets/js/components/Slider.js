let slideIndex = 1;

// check which slider wil be displayed

function SliderSelect() {
    const width = window.innerWidth;
    if (width < 951) {
        const slider = document.getElementById("mobile-slider");
        return slider;
    } else {
        const slider = document.getElementById("desktop-slider");
        return slider;
    }
}

const slider = SliderSelect();

if (document.location.pathname === "/") {
    showSlides(slideIndex);
}

// Automatic slides with pause functionality
const time = 7000;

export function AutomaticSlideAndSwipe() {
    function AutomaticSlide() {
        showSlides((slideIndex += 1));
    }

    let interval = setInterval(AutomaticSlide, time);

    function PauseSlides() {
        clearInterval(interval);
    }

    function ContinueSlides() {
        interval = setInterval(AutomaticSlide, time);
    }

    slider.onmouseenter = PauseSlides;
    slider.onmouseleave = ContinueSlides;

    slider.ontouchstart = HandleTouchStart;
    slider.ontouchmove = Swipe;

    // swipe functionality

    let touchstart = null;

    function getTouch(e) {
        return e.touches;
    }

    function HandleTouchStart(e) {
        const firstTouch = getTouch(e)[0];
        touchstart = firstTouch.clientX;
    }

    function Swipe(e) {
        if (!touchstart) {
            return;
        }
        PauseSlides();

        const touchend = e.touches[0].clientX;
        const Difference = touchstart - touchend;

        if (Difference > 0) {
            plusSlides(-1);
        } else {
            plusSlides(1);
        }

        touchstart = null;
        ContinueSlides();
    }
}

// Next/previous controls

export function plusSlides(n) {
    showSlides((slideIndex += n));
}

// Thumbnail image controls
export function currentSlide(n) {
    showSlides((slideIndex = n));
}

export function showSlides(n) {
    let i;
    const slides = slider.getElementsByClassName("slide");
    if (slides.length === 0) {
        return;
    }
    const dots = slider.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}
