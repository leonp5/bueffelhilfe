export default function ToggleDescriptionBox(id) {
    const descriptionRow = document.getElementById("description" + id);
    const arrow = document.getElementById("arrow" + id);

    if (arrow.style.transform === "rotate(0deg)") {
        arrow.style.transform = "rotate(-90deg)";
        descriptionRow.classList.remove("fadeIn");
        descriptionRow.classList.add("fadeOut");
        setTimeout(() => {
            descriptionRow.classList.remove("description-box-visible");
            descriptionRow.style.opacity = "0";
            descriptionRow.style.display = "none";
            descriptionRow.classList.remove("fadeOut");
        }, 300);
    } else {
        arrow.style.transform = "rotate(0deg)";
        descriptionRow.style.display = "initial";
        descriptionRow.classList.add("description-box-visible");
        descriptionRow.classList.add("fadeIn");
        setTimeout(() => {
            descriptionRow.style.opacity = "1";
        }, 300);
    }
}
