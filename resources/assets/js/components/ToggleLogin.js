export function ToggleDesktopLogin() {
    const position = window.pageYOffset;

    if (position > 80) {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth"
        });
        setTimeout(() => {
            toggleContainer();
        }, 750);
    } else {
        toggleContainer();
    }

    function toggleContainer() {
        const container = document.getElementById("login-container");
        if (container.style.display === "flex") {
            container.classList.remove("desktop-login-open");
            setTimeout(() => {
                container.style.display = "none";
            }, 500);
        } else {
            container.style.display = "flex";
            setTimeout(() => {
                container.classList.add("desktop-login-open");
            }, 10);
        }
    }
}

export function ToggleMobileLogin() {
    const modalBG = document.getElementById("modal-background");
    const modal = document.getElementById("login-modal");
    modalBG.classList.toggle("modal-background-open");
    if (modal.style.display === "initial") {
        modal.classList.remove("login-modal-open");
        setTimeout(() => {
            modal.style.display = "none";
        }, 600);
    } else {
        modal.style.display = "initial";
        setTimeout(() => {
            modal.classList.add("login-modal-open");
        }, 10);
    }
}
