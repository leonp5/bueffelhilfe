import { GetStarRating } from "./StarRating";

export default async function CollectData() {
    const form = document.getElementById("report-content");
    const name = form.querySelector(".report-input").value;
    const text = form.querySelector(".textarea").value;

    const rating = await GetStarRating();

    if (document.getElementsByName("visible")[0]) {
        const checkbox_visible = document.getElementsByName("visible")[0]
            .checked;
        const checkbox_slider = document.getElementsByName("show_in_slider")[0]
            .checked;

        function checkValue(value) {
            if (value === true) {
                const result = "1";
                return result;
            } else {
                const result = "0";
                return result;
            }
        }
        const visible = checkValue(checkbox_visible);
        const showInSlider = checkValue(checkbox_slider);

        const result = {
            name: name,
            body: text,
            visible: visible,
            show_in_slider: showInSlider,
            rating: rating,
            new: false
        };

        return result;
    } else {
        const result = {
            name: name,
            body: text,
            visible: "0",
            show_in_slider: "0",
            rating: rating,
            new: true
        };

        return result;
    }
}
