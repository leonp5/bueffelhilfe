export default function ValidateInput() {
    const minChars = 50;
    const form = document.getElementById("report-content");
    const textareaValue = form.getElementsByClassName("textarea")[0].value;
    const showRemainingChars = form.getElementsByClassName("small")[0];
    const button = document.getElementById("send-report");
    const nameInputValue = form.getElementsByClassName("report-input")[0].value;

    setTimeout(() => {
        const starRating = sessionStorage.getItem("rating");
        if (
            textareaValue.length <= minChars ||
            nameInputValue.length < 3 ||
            !starRating
        ) {
            if (textareaValue.length <= minChars) {
                const remaining = minChars - textareaValue.length;
                showRemainingChars.style.color = "red";
                showRemainingChars.textContent =
                    `Mindestens ` + remaining + ` Zeichen`;
            }
            if (nameInputValue.length < 3) {
                showRemainingChars.style.color = "red";
            }
            button.disabled = true;
        } else {
            showRemainingChars.style.color = "#049b04";
            button.disabled = false;
        }
    }, 50);
}
