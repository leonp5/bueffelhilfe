export default function StarRating() {
    const ratingbox = document.getElementById("rating");
    ratingbox.addEventListener("click", SetRating);
    function SetRating(e) {
        const rating = e.target.value;
        sessionStorage.setItem("rating", rating);
    }
}

export async function GetStarRating() {
    const rating = sessionStorage.getItem("rating");
    sessionStorage.removeItem("rating");
    return rating;
}
