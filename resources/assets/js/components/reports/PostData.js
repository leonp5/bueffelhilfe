import SaveData from "../api/SaveData";
import CollectData from "./CollectData";
import ToggleNewReport from "./ToggleNewReport";

function ReloadPage() {
    location.reload();
}

export default async function PostData() {
    try {
        const report = await CollectData();

        const response = await SaveData("/api/reports", report);

        const alert = document.createElement("div");
        alert.addEventListener("click", ReloadPage);

        if (response.success) {
            alert.className = "alert-modal";
            if (report.new === true) {
                alert.innerHTML =
                    "Vielen Dank für das Feedback! Ich schalte es zeitnah frei.";
            } else {
                alert.innerHTML = response.message;
            }
            ToggleNewReport();
            const modal = document.getElementById("report-content");
            modal.querySelector(".textarea").value = "";
            modal.querySelector(".report-input").value = "";
            const checkboxes = modal.querySelectorAll("input[type=checkbox]");
            checkboxes.forEach(checkbox => (checkbox.checked = false));
        } else {
            alert.className = "warning-modal";
            alert.innerHTML = response.message;
        }

        document.getElementById("content").appendChild(alert);
    } catch (error) {
        console.error(error);
    }
}
