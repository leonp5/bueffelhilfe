export default function ToggleNewReport() {
    const modal = document.querySelector(".report-modal");
    const modalcontent = document.querySelector(".report-modal-content");
    if (modal.style.display === "flex") {
        modal.style.opacity = "0";
        modalcontent.style.opacity = "0";
        setTimeout(() => {
            modal.style.width = "0px";
            modal.style.height = "0px";
            modal.style.display = "none";
        }, 500);
    } else {
        modal.style.display = "flex";
        setTimeout(() => {
            modal.style.opacity = "1";
            modal.style.width = "90%";
            modal.style.height = "80%";
        }, 10);
        setTimeout(() => {
            modalcontent.style.opacity = "1";
        }, 200);
    }
}
