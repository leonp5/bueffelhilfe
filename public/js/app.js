/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}


/***/ }),

/***/ "./resources/assets/js/app.js":
/*!************************************!*\
  !*** ./resources/assets/js/app.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_ToggleImage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/ToggleImage */ "./resources/assets/js/components/ToggleImage.js");
/* harmony import */ var _components_ToggleMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/ToggleMenu */ "./resources/assets/js/components/ToggleMenu.js");
/* harmony import */ var _components_ScrollTo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/ScrollTo */ "./resources/assets/js/components/ScrollTo.js");
/* harmony import */ var _components_ToggleLogin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/ToggleLogin */ "./resources/assets/js/components/ToggleLogin.js");
/* harmony import */ var _components_FadeLoginButton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/FadeLoginButton */ "./resources/assets/js/components/FadeLoginButton.js");
/* harmony import */ var _components_reports_PostData__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/reports/PostData */ "./resources/assets/js/components/reports/PostData.js");
/* harmony import */ var _components_reports_ToggleNewReport__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/reports/ToggleNewReport */ "./resources/assets/js/components/reports/ToggleNewReport.js");
/* harmony import */ var _components_reports_ValidateInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/reports/ValidateInput */ "./resources/assets/js/components/reports/ValidateInput.js");
/* harmony import */ var _components_reports_StarRating__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/reports/StarRating */ "./resources/assets/js/components/reports/StarRating.js");
/* harmony import */ var _components_Slider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/Slider */ "./resources/assets/js/components/Slider.js");
/* harmony import */ var _components_RemoveAlert__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/RemoveAlert */ "./resources/assets/js/components/RemoveAlert.js");
/* harmony import */ var _components_ToggleDescriptionBox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/ToggleDescriptionBox */ "./resources/assets/js/components/ToggleDescriptionBox.js");
/* harmony import */ var _components_ToggleDeleteModal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/ToggleDeleteModal */ "./resources/assets/js/components/ToggleDeleteModal.js");
/* harmony import */ var _components_CookieModal__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/CookieModal */ "./resources/assets/js/components/CookieModal.js");
/* harmony import */ var _components_files_ToggleMoveModal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/files/ToggleMoveModal */ "./resources/assets/js/components/files/ToggleMoveModal.js");
/* harmony import */ var _components_files_HandleFileUpload__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/files/HandleFileUpload */ "./resources/assets/js/components/files/HandleFileUpload.js");
/* harmony import */ var _components_files_DragNDrop__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/files/DragNDrop */ "./resources/assets/js/components/files/DragNDrop.js");
/* harmony import */ var _components_files_ShowAndHideButtons__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/files/ShowAndHideButtons */ "./resources/assets/js/components/files/ShowAndHideButtons.js");
/* harmony import */ var _components_files_ValidateSelectFolder__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/files/ValidateSelectFolder */ "./resources/assets/js/components/files/ValidateSelectFolder.js");
/* harmony import */ var _components_files_ValidateFolderName__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/files/ValidateFolderName */ "./resources/assets/js/components/files/ValidateFolderName.js");
/* harmony import */ var _components_files_ToggleSelectAll__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/files/ToggleSelectAll */ "./resources/assets/js/components/files/ToggleSelectAll.js");




















 // onload

window.onload = Object(_components_FadeLoginButton__WEBPACK_IMPORTED_MODULE_4__["default"])();
document.onload = Object(_components_CookieModal__WEBPACK_IMPORTED_MODULE_13__["CookieModal"])();

if (document.location.pathname === "/") {
  // slider
  window.onload = Object(_components_Slider__WEBPACK_IMPORTED_MODULE_9__["AutomaticSlideAndSwipe"])(); // for page navigation

  window.onload = Object(_components_ScrollTo__WEBPACK_IMPORTED_MODULE_2__["GetPathIDfromSessionStorage"])();
} // get star rating value


if (document.body.contains(document.getElementById("rating"))) {
  window.onload = Object(_components_reports_StarRating__WEBPACK_IMPORTED_MODULE_8__["default"])();
}

document.ToggleMenu = _components_ToggleMenu__WEBPACK_IMPORTED_MODULE_1__["default"];
document.ScrollTo = _components_ScrollTo__WEBPACK_IMPORTED_MODULE_2__["ScrollTo"];
window.ToggleMobileLogin = _components_ToggleLogin__WEBPACK_IMPORTED_MODULE_3__["ToggleMobileLogin"];
window.ToggleDesktopLogin = _components_ToggleLogin__WEBPACK_IMPORTED_MODULE_3__["ToggleDesktopLogin"];
window.PostData = _components_reports_PostData__WEBPACK_IMPORTED_MODULE_5__["default"];
window.ToggleNewReport = _components_reports_ToggleNewReport__WEBPACK_IMPORTED_MODULE_6__["default"];
document.RemoveAlert = _components_RemoveAlert__WEBPACK_IMPORTED_MODULE_10__["default"]; // landing page

if (document.location.pathname === "/") {
  window.ToggleImage = _components_ToggleImage__WEBPACK_IMPORTED_MODULE_0__["default"];
  document.showSlides = _components_Slider__WEBPACK_IMPORTED_MODULE_9__["showSlides"];
  document.plusSlides = _components_Slider__WEBPACK_IMPORTED_MODULE_9__["plusSlides"];
  document.currentSlide = _components_Slider__WEBPACK_IMPORTED_MODULE_9__["currentSlide"];
} // if (
//     document.location.pathname === "/admin/folder/create" ||
//     "/admin/file/create"
// ) {
//     window.onload = CookieModal();
// }
// charValidation


if (document.body.contains(document.getElementById("validation-necessary"))) {
  window.ValidateInput = _components_reports_ValidateInput__WEBPACK_IMPORTED_MODULE_7__["default"];
} // validate file upload


if (document.body.contains(document.getElementById("file-input"))) {
  window.onload = function () {
    Object(_components_files_HandleFileUpload__WEBPACK_IMPORTED_MODULE_15__["default"])();
    Object(_components_files_DragNDrop__WEBPACK_IMPORTED_MODULE_16__["default"])();
  };

  window.HandleFileUpload = _components_files_HandleFileUpload__WEBPACK_IMPORTED_MODULE_15__["default"];
} // toggle folder description


if (document.location.pathname === "/folders") {
  window.ToggleDescriptionBox = _components_ToggleDescriptionBox__WEBPACK_IMPORTED_MODULE_11__["default"];
} // toggle file description


if (window.location.href.indexOf("files") > -1) {
  window.ToggleDescriptionBox = _components_ToggleDescriptionBox__WEBPACK_IMPORTED_MODULE_11__["default"];
} // toggle user delete modal


if (document.body.contains(document.getElementById("toggleDeleteIsNecessary"))) {
  window.ToggleDeleteModal = _components_ToggleDeleteModal__WEBPACK_IMPORTED_MODULE_12__["default"];
} // validate folder create/edit form name


if (document.body.contains(document.getElementById("folderNameCheckNecessary"))) {
  window.onload = _components_files_ValidateFolderName__WEBPACK_IMPORTED_MODULE_19__["default"];
  window.ValidateFolderName = _components_files_ValidateFolderName__WEBPACK_IMPORTED_MODULE_19__["default"];
} // show and hide delete/move file buttons


if (document.body.contains(document.getElementById("ShowAndHideButtonsNecessary"))) {
  window.ShowAndHideButtons = _components_files_ShowAndHideButtons__WEBPACK_IMPORTED_MODULE_17__["default"];
}

if (document.body.contains(document.getElementById("cookie-modal"))) {
  window.ConfirmCookie = _components_CookieModal__WEBPACK_IMPORTED_MODULE_13__["ConfirmCookie"];
}

if (document.body.contains(document.getElementById("floating-move"))) {
  window.ToggleMoveModal = _components_files_ToggleMoveModal__WEBPACK_IMPORTED_MODULE_14__["default"];
  window.ValidateSelectFolder = _components_files_ValidateSelectFolder__WEBPACK_IMPORTED_MODULE_18__["default"];
}

if (document.body.contains(document.getElementById("toggleSelectAllIsNecessary"))) {
  window.ToggleSelectAll = _components_files_ToggleSelectAll__WEBPACK_IMPORTED_MODULE_20__["default"];
}

/***/ }),

/***/ "./resources/assets/js/components/CookieModal.js":
/*!*******************************************************!*\
  !*** ./resources/assets/js/components/CookieModal.js ***!
  \*******************************************************/
/*! exports provided: CookieModal, ConfirmCookie */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieModal", function() { return CookieModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmCookie", function() { return ConfirmCookie; });
function CookieModal() {
  document.getElementById("js-hint").remove();
  var cookieAccepted = localStorage.getItem("bueffelCookie");

  if (cookieAccepted === null) {
    var modalBG = document.createElement("div");
    modalBG.setAttribute("id", "cookie-modal");
    modalBG.className = "modal-background modal-background-open";
    var modal = document.createElement("div");
    modal.className = "cookie-modal";
    modalBG.appendChild(modal);
    var text = document.createElement("p");
    text.className = "center-text";
    text.innerHTML = "Diese Seite verwendet ausschlie\xDFlich technische Cookies, die f\xFCr den Betrieb der Website\n        notwendig sind. <br> Es werden keine personenbezogenen Daten erhoben.";
    modal.appendChild(text);
    var submitButton = document.createElement("button");
    submitButton.className = "basic-btn margin-20";
    submitButton.innerHTML = "In Ordnung!";
    submitButton.onclick = ConfirmCookie;
    modal.appendChild(submitButton);
    var mainContainer = document.getElementById("main-container");
    mainContainer.appendChild(modalBG);
  }
}
function ConfirmCookie() {
  localStorage.setItem("bueffelCookie", 1);
  var modalBG = document.getElementById("cookie-modal");
  modalBG.classList.remove("modal-background-open");
  modalBG.remove();
}

/***/ }),

/***/ "./resources/assets/js/components/FadeLoginButton.js":
/*!***********************************************************!*\
  !*** ./resources/assets/js/components/FadeLoginButton.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FadeLoginButton; });
function FadeLoginButton() {
  var width = window.innerWidth;

  if (width < 950) {
    document.addEventListener("touchstart", HandleTouchStart);
    document.addEventListener("touchmove", CheckScrollPosition);
  }
}
var touchstart = null;

function HandleTouchStart(e) {
  touchstart = e.touches[0].clientY;
}

function CheckScrollPosition(e) {
  if (e.touches.length === 1) {
    var touchend = e.changedTouches[0].clientY;
    var button = document.getElementById("mobile-login-btn");

    if (touchstart > touchend) {
      button.classList.add("floating-btn-show");
    } else {
      button.classList.remove("floating-btn-show");
    }
  }
}

/***/ }),

/***/ "./resources/assets/js/components/RemoveAlert.js":
/*!*******************************************************!*\
  !*** ./resources/assets/js/components/RemoveAlert.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RemoveAlert; });
function RemoveAlert() {
  var alert = document.querySelector("div[class=alert-modal]");
  var warning = document.querySelector("div[class=warning-modal]");

  if (alert) {
    alert.style.display = "none";
  }

  if (warning) {
    warning.style.display = "none";
  }
}

/***/ }),

/***/ "./resources/assets/js/components/ScrollTo.js":
/*!****************************************************!*\
  !*** ./resources/assets/js/components/ScrollTo.js ***!
  \****************************************************/
/*! exports provided: ScrollTo, GetPathIDfromSessionStorage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollTo", function() { return ScrollTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPathIDfromSessionStorage", function() { return GetPathIDfromSessionStorage; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ScrollTo(_x) {
  return _ScrollTo.apply(this, arguments);
}

function _ScrollTo() {
  _ScrollTo = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(id) {
    var currentPath;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            currentPath = document.location.pathname;

            if (currentPath !== "/") {
              sessionStorage.setItem("id", JSON.stringify(id));
              document.location.pathname = "/";
            } else {
              scroll(id);
              document.getElementById("mobile-menu").classList.remove("mobile-menu-open");
            }

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _ScrollTo.apply(this, arguments);
}

function GetPathIDfromSessionStorage() {
  try {
    var item = sessionStorage.getItem("id");

    if (item) {
      var id = JSON.parse(item);
      scroll(id);
      sessionStorage.removeItem("id");
    }
  } catch (error) {
    console.error(error);
  }
}

function scroll(id) {
  history.pushState(null, null, "#" + id);
  ChangeLinkStyle(id);
  var currentSection = document.getElementById(id);
  currentSection.scrollIntoView({
    behavior: "smooth"
  });
}

function ChangeLinkStyle(id) {
  var navbar = document.getElementById("navbar");
  var oldLink = navbar.querySelector("a[class=navbar-link-active]");
  oldLink.classList.remove("navbar-link-active");
  oldLink.classList.add("navbar-link");
  var currentLink = navbar.querySelector("a[name=".concat(id));
  currentLink.classList.remove("navbar-link");
  currentLink.classList.add("navbar-link-active");
} // function IsElementInViewport(el) {
//     const rect = el.getBoundingClientRect();
//     return (
//         rect.bottom < 0 ||
//         rect.right < 0 ||
//         rect.left > window.innerWidth ||
//         rect.top > window.innerHeight
//     );
// }
// function OnVisibilityChange(id) {
//     let old_visible;
//     const el = document.getElementById(id);
//     const visible = IsElementInViewport(el);
//     if (visible != old_visible) {
//         old_visible = visible;
//         ChangeLinkStyle(id);
//     }
// }

/***/ }),

/***/ "./resources/assets/js/components/Slider.js":
/*!**************************************************!*\
  !*** ./resources/assets/js/components/Slider.js ***!
  \**************************************************/
/*! exports provided: AutomaticSlideAndSwipe, plusSlides, currentSlide, showSlides */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutomaticSlideAndSwipe", function() { return AutomaticSlideAndSwipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "plusSlides", function() { return plusSlides; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentSlide", function() { return currentSlide; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showSlides", function() { return showSlides; });
var slideIndex = 1; // check which slider wil be displayed

function SliderSelect() {
  var width = window.innerWidth;

  if (width < 951) {
    var _slider = document.getElementById("mobile-slider");

    return _slider;
  } else {
    var _slider2 = document.getElementById("desktop-slider");

    return _slider2;
  }
}

var slider = SliderSelect();

if (document.location.pathname === "/") {
  showSlides(slideIndex);
} // Automatic slides with pause functionality


var time = 7000;
function AutomaticSlideAndSwipe() {
  function AutomaticSlide() {
    showSlides(slideIndex += 1);
  }

  var interval = setInterval(AutomaticSlide, time);

  function PauseSlides() {
    clearInterval(interval);
  }

  function ContinueSlides() {
    interval = setInterval(AutomaticSlide, time);
  }

  slider.onmouseenter = PauseSlides;
  slider.onmouseleave = ContinueSlides;
  slider.ontouchstart = HandleTouchStart;
  slider.ontouchmove = Swipe; // swipe functionality

  var touchstart = null;

  function getTouch(e) {
    return e.touches;
  }

  function HandleTouchStart(e) {
    var firstTouch = getTouch(e)[0];
    touchstart = firstTouch.clientX;
  }

  function Swipe(e) {
    if (!touchstart) {
      return;
    }

    PauseSlides();
    var touchend = e.touches[0].clientX;
    var Difference = touchstart - touchend;

    if (Difference > 0) {
      plusSlides(-1);
    } else {
      plusSlides(1);
    }

    touchstart = null;
    ContinueSlides();
  }
} // Next/previous controls

function plusSlides(n) {
  showSlides(slideIndex += n);
} // Thumbnail image controls

function currentSlide(n) {
  showSlides(slideIndex = n);
}
function showSlides(n) {
  var i;
  var slides = slider.getElementsByClassName("slide");

  if (slides.length === 0) {
    return;
  }

  var dots = slider.getElementsByClassName("dot");

  if (n > slides.length) {
    slideIndex = 1;
  }

  if (n < 1) {
    slideIndex = slides.length;
  }

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
}

/***/ }),

/***/ "./resources/assets/js/components/ToggleDeleteModal.js":
/*!*************************************************************!*\
  !*** ./resources/assets/js/components/ToggleDeleteModal.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleDeleteModal; });
function ToggleDeleteModal(data) {
  var modalBG = document.getElementById("modal-background");
  modalBG.classList.toggle("modal-background-open");

  if (document.getElementById("delete-modal")) {
    document.getElementById("delete-modal").remove();
  } else {
    var modal = document.createElement("div");
    modal.setAttribute("id", "delete-modal");
    modal.className = "delete-modal";
    var closeButton = document.createElement("input");
    modal.innerHTML = "\"".concat(data.name, "\"") + " mit allen Dateien unwideruflich löschen?";
    closeButton.className = "close-button";
    closeButton.onclick = ToggleDeleteModal;
    modal.appendChild(closeButton);
    var closeCross = document.createElement("a");
    closeCross.className = "close-cross";
    modal.appendChild(closeCross);
    var row = document.createElement("div");
    row.className = "row-space-between";
    var abortButton = document.createElement("button");
    abortButton.className = "abort-btn";
    abortButton.innerHTML = "Abbrechen";
    abortButton.onclick = ToggleDeleteModal;
    row.appendChild(abortButton);
    var deleteForm = document.createElement("form");
    deleteForm.setAttribute("method", "post");
    deleteForm.setAttribute("accept-charset", "UTF-8"); // if the route at web.php gets changed, maybe this line must be modified to get the right url

    var url = window.location.href;
    deleteForm.setAttribute("action", url + "/" + data.id);
    var hiddenInput_1 = document.createElement("input");
    hiddenInput_1.setAttribute("name", "_method");
    hiddenInput_1.setAttribute("type", "hidden");
    hiddenInput_1.setAttribute("value", "DELETE");
    deleteForm.appendChild(hiddenInput_1);
    var hiddenInput_2 = document.createElement("input");
    hiddenInput_2.setAttribute("name", "_token");
    hiddenInput_2.setAttribute("type", "hidden");
    var token = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
    hiddenInput_2.setAttribute("value", token);
    deleteForm.appendChild(hiddenInput_2);
    row.appendChild(deleteForm);
    var submitButton = document.createElement("button");
    submitButton.className = "delete-btn";
    submitButton.innerHTML = "Löschen";
    deleteForm.appendChild(submitButton);
    modal.appendChild(row);
    modalBG.appendChild(modal);
  }
}

/***/ }),

/***/ "./resources/assets/js/components/ToggleDescriptionBox.js":
/*!****************************************************************!*\
  !*** ./resources/assets/js/components/ToggleDescriptionBox.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleDescriptionBox; });
function ToggleDescriptionBox(id) {
  var descriptionRow = document.getElementById("description" + id);
  var arrow = document.getElementById("arrow" + id);

  if (arrow.style.transform === "rotate(0deg)") {
    arrow.style.transform = "rotate(-90deg)";
    descriptionRow.classList.remove("fadeIn");
    descriptionRow.classList.add("fadeOut");
    setTimeout(function () {
      descriptionRow.classList.remove("description-box-visible");
      descriptionRow.style.opacity = "0";
      descriptionRow.style.display = "none";
      descriptionRow.classList.remove("fadeOut");
    }, 300);
  } else {
    arrow.style.transform = "rotate(0deg)";
    descriptionRow.style.display = "initial";
    descriptionRow.classList.add("description-box-visible");
    descriptionRow.classList.add("fadeIn");
    setTimeout(function () {
      descriptionRow.style.opacity = "1";
    }, 300);
  }
}

/***/ }),

/***/ "./resources/assets/js/components/ToggleImage.js":
/*!*******************************************************!*\
  !*** ./resources/assets/js/components/ToggleImage.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleImage; });
function ToggleImage() {
  var modalBG = document.getElementById("img-modal-background");
  var modalIMG = document.querySelector(".modal-img");
  var modalContent = document.querySelector(".img-modal-content");
  var img = "storage/images/david_menrath_bueffelhilfe.jpg";

  if (modalBG.style.display === "flex") {
    modalBG.style.opacity = "0";
    modalContent.style.transform = "scale(0)";
    setTimeout(function () {
      modalBG.style.display = "none";
      modalIMG.style.display = "none";
      modalIMG.setAttribute("src", "");
    }, 500);
  } else {
    modalBG.style.display = "flex";
    modalIMG.style.display = "initial";
    modalIMG.setAttribute("src", img);
    setTimeout(function () {
      modalBG.style.opacity = "1";
      modalContent.style.transform = "scale(1)";
    }, 10);
  }
}

/***/ }),

/***/ "./resources/assets/js/components/ToggleLogin.js":
/*!*******************************************************!*\
  !*** ./resources/assets/js/components/ToggleLogin.js ***!
  \*******************************************************/
/*! exports provided: ToggleDesktopLogin, ToggleMobileLogin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToggleDesktopLogin", function() { return ToggleDesktopLogin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToggleMobileLogin", function() { return ToggleMobileLogin; });
function ToggleDesktopLogin() {
  var position = window.pageYOffset;

  if (position > 80) {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
    setTimeout(function () {
      toggleContainer();
    }, 750);
  } else {
    toggleContainer();
  }

  function toggleContainer() {
    var container = document.getElementById("login-container");

    if (container.style.display === "flex") {
      container.classList.remove("desktop-login-open");
      setTimeout(function () {
        container.style.display = "none";
      }, 500);
    } else {
      container.style.display = "flex";
      setTimeout(function () {
        container.classList.add("desktop-login-open");
      }, 10);
    }
  }
}
function ToggleMobileLogin() {
  var modalBG = document.getElementById("modal-background");
  var modal = document.getElementById("login-modal");
  modalBG.classList.toggle("modal-background-open");

  if (modal.style.display === "initial") {
    modal.classList.remove("login-modal-open");
    setTimeout(function () {
      modal.style.display = "none";
    }, 600);
  } else {
    modal.style.display = "initial";
    setTimeout(function () {
      modal.classList.add("login-modal-open");
    }, 10);
  }
}

/***/ }),

/***/ "./resources/assets/js/components/ToggleMenu.js":
/*!******************************************************!*\
  !*** ./resources/assets/js/components/ToggleMenu.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleMenu; });
function ToggleMenu() {
  var menu = document.getElementById("mobile-menu");

  if (menu.style.display === "flex") {
    document.removeEventListener("click", ClickOutside, true);
    menu.classList.remove("mobile-menu-open");
    setTimeout(function () {
      menu.style.display = "none";
    }, 600);
  } else {
    menu.style.display = "flex";
    document.addEventListener("click", ClickOutside, true);
    setTimeout(function () {
      menu.classList.add("mobile-menu-open");
    }, 10);
  }
}

function ClickOutside(event) {
  var menu = document.getElementById("mobile-menu");

  if (event.target !== menu) {
    ToggleMenu();
  }
}

/***/ }),

/***/ "./resources/assets/js/components/api/SaveData.js":
/*!********************************************************!*\
  !*** ./resources/assets/js/components/api/SaveData.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SaveData; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function SaveData(_x, _x2) {
  return _SaveData.apply(this, arguments);
}

function _SaveData() {
  _SaveData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(api, data) {
    var token, url, response, result;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            token = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            url = window.location.origin + api;
            _context.next = 4;
            return fetch(url, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": token,
                Accept: "application/json"
              },
              body: JSON.stringify(data)
            });

          case 4:
            response = _context.sent;
            _context.next = 7;
            return response.json();

          case 7:
            result = _context.sent;
            return _context.abrupt("return", result);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _SaveData.apply(this, arguments);
}

/***/ }),

/***/ "./resources/assets/js/components/files/DragNDrop.js":
/*!***********************************************************!*\
  !*** ./resources/assets/js/components/files/DragNDrop.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DragNDrop; });
/* harmony import */ var _HandleFileUpload__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HandleFileUpload */ "./resources/assets/js/components/files/HandleFileUpload.js");

function DragNDrop() {
  // check if the browser supports drag and drop
  var canDragNDrop = function () {
    var div = document.createElement("div");
    return ("draggable" in div || "ondragstart" in div && "ondrop" in div) && "FormData" in window && "FileReader" in window;
  }();

  if (canDragNDrop) {
    var preventDefault = function preventDefault(e) {
      e.preventDefault();
      e.stopPropagation();
    };

    var dragOver = function dragOver(e) {
      preventDefault(e);
      box.classList.add("dragover");
    };

    var dragLeave = function dragLeave(e) {
      preventDefault(e);
      box.classList.remove("dragover");
    };

    var handleDrop = function handleDrop(e) {
      dragLeave(e);
      var fileInput = document.getElementById("file-input");
      var dataTransfer = new DataTransfer();
      dataTransfer.items.add(e.dataTransfer.files[0]);
      fileInput.files = dataTransfer.files;
      Object(_HandleFileUpload__WEBPACK_IMPORTED_MODULE_0__["default"])();
    };

    var box = document.getElementById("dragNdrop");
    box.classList.add("dragNdrop-avalaible");
    box.addEventListener("drag", preventDefault);
    box.addEventListener("dragstart", preventDefault);
    box.addEventListener("dragover", dragOver);
    box.addEventListener("dragenter", dragOver);
    box.addEventListener("dragend", dragLeave);
    box.addEventListener("dragleave", dragLeave);
    box.addEventListener("drop", handleDrop);
  }
}

/***/ }),

/***/ "./resources/assets/js/components/files/HandleFileUpload.js":
/*!******************************************************************!*\
  !*** ./resources/assets/js/components/files/HandleFileUpload.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HandleFileUpload; });
function HandleFileUpload() {
  // Show filename of (new) uploaded file & check if savedFile exists
  var fileInputName = document.getElementById("file-input").value;
  var savedFileExists = document.getElementById("saved-file");
  var submitButton = document.getElementById("FileUploadSubmit");

  function HandleFileInput() {
    if (fileInputName !== "") {
      var fileSelected = document.getElementById("file-selected");
      var fileName = fileInputName.split("\\").pop();
      fileSelected.innerHTML = fileName;
      fileSelected.style.color = "#049b04";

      if (savedFileExists && fileInputName !== "") {
        var oldFileText = document.getElementById("old-file-text");
        oldFileText.style.color = "red";
      }

      return true;
    } else {
      return false;
    }
  }

  var fileInput = HandleFileInput(); // check, which upload form is rendered

  var userNameInput = document.getElementById("username");

  if (userNameInput) {
    var selectedUserName = userNameInput.options[userNameInput.selectedIndex].value;
    var folderInput = document.getElementById("folder");
    var selectedFolder = folderInput.options[folderInput.selectedIndex].value; // manage enabling & disabling submit button

    if (selectedFolder) {
      userNameInput.disabled = true;
    }

    if (selectedFolder === "") {
      userNameInput.disabled = false;
    }

    if (selectedUserName) {
      folderInput.disabled = true;
    }

    if (selectedUserName === "") {
      folderInput.disabled = false;
    }

    if ((selectedUserName || selectedFolder) && (fileInput === true || savedFileExists)) {
      submitButton.disabled = false;
    } else {
      submitButton.disabled = true;
    }
  } else {
    if (fileInput === true) {
      submitButton.disabled = false;
    } else {
      submitButton.disabled = true;
    }
  }
}

/***/ }),

/***/ "./resources/assets/js/components/files/ShowAndHideButtons.js":
/*!********************************************************************!*\
  !*** ./resources/assets/js/components/files/ShowAndHideButtons.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ShowAndHideButtons; });
function ShowAndHideButtons() {
  // get the inputs which contains "files" in the name attribute AND have type = checkbox
  var checkboxes = document.querySelectorAll("input[name^=files][type=checkbox]");
  var deleteButton = document.getElementById("floating-delete");
  var moveButton = document.getElementById("floating-move");
  var container = document.getElementById("toggleSelectAllIsNecessary");
  var toggleAll = container.getElementsByTagName("input")[0];
  var text = container.getElementsByTagName("p")[0]; // You need the Array.prototype.slice.call part to convert the NodeList returned by document.querySelectorAll into an array that you can call some on.

  var checkedOne = Array.prototype.slice.call(checkboxes).some(function (x) {
    return x.checked;
  });

  if (checkedOne) {
    deleteButton.style.display = "flex";

    if (moveButton) {
      moveButton.style.display = "flex";
    }
  } else {
    deleteButton.style.display = "none";

    if (moveButton) {
      moveButton.style.display = "none";
    }
  }

  var unCheckedOne = Array.prototype.slice.call(checkboxes).some(function (x) {
    return !x.checked;
  });

  if (unCheckedOne) {
    text.innerHTML = "Alle auswählen";
    toggleAll.checked = false;
  }

  if (!unCheckedOne) {
    toggleAll.checked = true;
    text.innerHTML = "Alle abwählen";
  }

  var i = 0;

  for (i = 0; i < checkboxes.length; ++i) {
    var id = checkboxes[i].id;
    var folder = document.querySelector("div[id=\"".concat(id, "\"]"));

    if (checkboxes[i].checked) {
      folder.style.background = "#00000080";
    } else {
      folder.style.background = "initial";
    }
  }
}

/***/ }),

/***/ "./resources/assets/js/components/files/ToggleMoveModal.js":
/*!*****************************************************************!*\
  !*** ./resources/assets/js/components/files/ToggleMoveModal.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleMoveModal; });
function ToggleMoveModal() {
  var form = document.getElementById("editAndDeleteForm");
  var input = form.querySelector("input[type=hidden][name=_method]");
  var modalBG = document.getElementById("modal-background");
  modalBG.classList.toggle("modal-background-open");
  var modal = document.getElementById("move-modal");
  var folderInput = document.getElementById("folder");
  folderInput.value = "";

  if (modal.style.display === "flex") {
    modal.style.display = "none";
    input.value = "DELETE";
  } else {
    modal.style.display = "flex";
    input.value = "PUT";
  }
}

/***/ }),

/***/ "./resources/assets/js/components/files/ToggleSelectAll.js":
/*!*****************************************************************!*\
  !*** ./resources/assets/js/components/files/ToggleSelectAll.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleSelectAll; });
/* harmony import */ var _ShowAndHideButtons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowAndHideButtons */ "./resources/assets/js/components/files/ShowAndHideButtons.js");

function ToggleSelectAll() {
  var checkboxes = document.querySelectorAll("input[name^=files][type=checkbox]");
  var container = document.getElementById("toggleSelectAllIsNecessary");
  var toggleAll = container.getElementsByTagName("input")[0].checked;
  var text = container.getElementsByTagName("p")[0];
  var i = 0;

  for (i = 0; i < checkboxes.length; ++i) {
    if (toggleAll === true) {
      checkboxes[i].checked = true;
      text.innerHTML = "Alle abwählen";
    }

    if (toggleAll === false) {
      checkboxes[i].checked = false;
      text.innerHTML = "Alle auswählen";
    }
  }

  Object(_ShowAndHideButtons__WEBPACK_IMPORTED_MODULE_0__["default"])();
}

/***/ }),

/***/ "./resources/assets/js/components/files/ValidateFolderName.js":
/*!********************************************************************!*\
  !*** ./resources/assets/js/components/files/ValidateFolderName.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ValidateFolderName; });
function ValidateFolderName() {
  var folderName = document.getElementById("folderNameCheckNecessary");
  var submitButton = document.getElementsByClassName("action-btn")[0];

  if (folderName.value.length < 3) {
    submitButton.disabled = true;
  } else {
    submitButton.disabled = false;
  }
}

/***/ }),

/***/ "./resources/assets/js/components/files/ValidateSelectFolder.js":
/*!**********************************************************************!*\
  !*** ./resources/assets/js/components/files/ValidateSelectFolder.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ValidateSelectFolder; });
function ValidateSelectFolder() {
  var button = document.getElementById("move-files-btn");
  var folderInput = document.getElementById("folder");
  var selectedFolder = folderInput.options[folderInput.selectedIndex].value;

  if (selectedFolder === "") {
    button.disabled = true;
  } else {
    button.disabled = false;
  }
}

/***/ }),

/***/ "./resources/assets/js/components/reports/CollectData.js":
/*!***************************************************************!*\
  !*** ./resources/assets/js/components/reports/CollectData.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectData; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _StarRating__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StarRating */ "./resources/assets/js/components/reports/StarRating.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


function CollectData() {
  return _CollectData.apply(this, arguments);
}

function _CollectData() {
  _CollectData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var form, name, text, rating, checkValue, checkbox_visible, checkbox_slider, visible, showInSlider, result, _result3;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            form = document.getElementById("report-content");
            name = form.querySelector(".report-input").value;
            text = form.querySelector(".textarea").value;
            _context.next = 5;
            return Object(_StarRating__WEBPACK_IMPORTED_MODULE_1__["GetStarRating"])();

          case 5:
            rating = _context.sent;

            if (!document.getElementsByName("visible")[0]) {
              _context.next = 16;
              break;
            }

            checkValue = function checkValue(value) {
              if (value === true) {
                var _result = "1";
                return _result;
              } else {
                var _result2 = "0";
                return _result2;
              }
            };

            checkbox_visible = document.getElementsByName("visible")[0].checked;
            checkbox_slider = document.getElementsByName("show_in_slider")[0].checked;
            visible = checkValue(checkbox_visible);
            showInSlider = checkValue(checkbox_slider);
            result = {
              name: name,
              body: text,
              visible: visible,
              show_in_slider: showInSlider,
              rating: rating,
              "new": false
            };
            return _context.abrupt("return", result);

          case 16:
            _result3 = {
              name: name,
              body: text,
              visible: "0",
              show_in_slider: "0",
              rating: rating,
              "new": true
            };
            return _context.abrupt("return", _result3);

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _CollectData.apply(this, arguments);
}

/***/ }),

/***/ "./resources/assets/js/components/reports/PostData.js":
/*!************************************************************!*\
  !*** ./resources/assets/js/components/reports/PostData.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PostData; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api_SaveData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api/SaveData */ "./resources/assets/js/components/api/SaveData.js");
/* harmony import */ var _CollectData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CollectData */ "./resources/assets/js/components/reports/CollectData.js");
/* harmony import */ var _ToggleNewReport__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ToggleNewReport */ "./resources/assets/js/components/reports/ToggleNewReport.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function ReloadPage() {
  location.reload();
}

function PostData() {
  return _PostData.apply(this, arguments);
}

function _PostData() {
  _PostData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var report, response, alert, modal, checkboxes;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return Object(_CollectData__WEBPACK_IMPORTED_MODULE_2__["default"])();

          case 3:
            report = _context.sent;
            _context.next = 6;
            return Object(_api_SaveData__WEBPACK_IMPORTED_MODULE_1__["default"])("/api/reports", report);

          case 6:
            response = _context.sent;
            alert = document.createElement("div");
            alert.addEventListener("click", ReloadPage);

            if (response.success) {
              alert.className = "alert-modal";

              if (report["new"] === true) {
                alert.innerHTML = "Vielen Dank für das Feedback! Ich schalte es zeitnah frei.";
              } else {
                alert.innerHTML = response.message;
              }

              Object(_ToggleNewReport__WEBPACK_IMPORTED_MODULE_3__["default"])();
              modal = document.getElementById("report-content");
              modal.querySelector(".textarea").value = "";
              modal.querySelector(".report-input").value = "";
              checkboxes = modal.querySelectorAll("input[type=checkbox]");
              checkboxes.forEach(function (checkbox) {
                return checkbox.checked = false;
              });
            } else {
              alert.className = "warning-modal";
              alert.innerHTML = response.message;
            }

            document.getElementById("content").appendChild(alert);
            _context.next = 16;
            break;

          case 13:
            _context.prev = 13;
            _context.t0 = _context["catch"](0);
            console.error(_context.t0);

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 13]]);
  }));
  return _PostData.apply(this, arguments);
}

/***/ }),

/***/ "./resources/assets/js/components/reports/StarRating.js":
/*!**************************************************************!*\
  !*** ./resources/assets/js/components/reports/StarRating.js ***!
  \**************************************************************/
/*! exports provided: default, GetStarRating */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return StarRating; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetStarRating", function() { return GetStarRating; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function StarRating() {
  var ratingbox = document.getElementById("rating");
  ratingbox.addEventListener("click", SetRating);

  function SetRating(e) {
    var rating = e.target.value;
    sessionStorage.setItem("rating", rating);
  }
}
function GetStarRating() {
  return _GetStarRating.apply(this, arguments);
}

function _GetStarRating() {
  _GetStarRating = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var rating;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            rating = sessionStorage.getItem("rating");
            sessionStorage.removeItem("rating");
            return _context.abrupt("return", rating);

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _GetStarRating.apply(this, arguments);
}

/***/ }),

/***/ "./resources/assets/js/components/reports/ToggleNewReport.js":
/*!*******************************************************************!*\
  !*** ./resources/assets/js/components/reports/ToggleNewReport.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToggleNewReport; });
function ToggleNewReport() {
  var modal = document.querySelector(".report-modal");
  var modalcontent = document.querySelector(".report-modal-content");

  if (modal.style.display === "flex") {
    modal.style.opacity = "0";
    modalcontent.style.opacity = "0";
    setTimeout(function () {
      modal.style.width = "0px";
      modal.style.height = "0px";
      modal.style.display = "none";
    }, 500);
  } else {
    modal.style.display = "flex";
    setTimeout(function () {
      modal.style.opacity = "1";
      modal.style.width = "90%";
      modal.style.height = "80%";
    }, 10);
    setTimeout(function () {
      modalcontent.style.opacity = "1";
    }, 200);
  }
}

/***/ }),

/***/ "./resources/assets/js/components/reports/ValidateInput.js":
/*!*****************************************************************!*\
  !*** ./resources/assets/js/components/reports/ValidateInput.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ValidateInput; });
function ValidateInput() {
  var minChars = 50;
  var form = document.getElementById("report-content");
  var textareaValue = form.getElementsByClassName("textarea")[0].value;
  var showRemainingChars = form.getElementsByClassName("small")[0];
  var button = document.getElementById("send-report");
  var nameInputValue = form.getElementsByClassName("report-input")[0].value;
  setTimeout(function () {
    var starRating = sessionStorage.getItem("rating");

    if (textareaValue.length <= minChars || nameInputValue.length < 3 || !starRating) {
      if (textareaValue.length <= minChars) {
        var remaining = minChars - textareaValue.length;
        showRemainingChars.style.color = "red";
        showRemainingChars.textContent = "Mindestens " + remaining + " Zeichen";
      }

      if (nameInputValue.length < 3) {
        showRemainingChars.style.color = "red";
      }

      button.disabled = true;
    } else {
      showRemainingChars.style.color = "#049b04";
      button.disabled = false;
    }
  }, 50);
}

/***/ }),

/***/ "./resources/assets/sass/app.scss":
/*!****************************************!*\
  !*** ./resources/assets/sass/app.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***************************************************************************!*\
  !*** multi ./resources/assets/js/app.js ./resources/assets/sass/app.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/neo/Projekte/bueffelhilfe/resources/assets/js/app.js */"./resources/assets/js/app.js");
module.exports = __webpack_require__(/*! /home/neo/Projekte/bueffelhilfe/resources/assets/sass/app.scss */"./resources/assets/sass/app.scss");


/***/ })

/******/ });